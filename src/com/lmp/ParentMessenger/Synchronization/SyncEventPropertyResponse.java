package com.lmp.ParentMessenger.Synchronization;

public class SyncEventPropertyResponse {
    public SyncEventPropertyTypeResponse type;
    public Boolean is_bag;
    public Object value;
}
