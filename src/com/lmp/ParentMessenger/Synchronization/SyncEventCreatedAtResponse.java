package com.lmp.ParentMessenger.Synchronization;

public class SyncEventCreatedAtResponse {
    public String date;
    public String timezone_type;
    public String timezone;
}
