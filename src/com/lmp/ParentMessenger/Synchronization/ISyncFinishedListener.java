package com.lmp.ParentMessenger.Synchronization;

public interface ISyncFinishedListener {
    public void SyncFinished(int totalCount, int successCount, String type);
}
