package com.lmp.ParentMessenger.Synchronization;

import android.content.Context;
import android.view.MenuItem;

public class SyncTaskInput {
    public Context context;
    public Boolean isSynchnonizing;
    public String listenerExternalId;

    public String type;

    public static String GeneralType = "general";

    private int mUpdatesCount = 0;
    private int mTotalUpdatesCount = 0;
    private int mSuccessCount = 0;
    private ISyncFinishedListener mListener;

    public int getUpdatesCount(){
        return mTotalUpdatesCount;
    }

    public SyncTaskInput(ISyncFinishedListener listener){
        mListener = listener;
    }

    public synchronized void decrement(boolean isSuccess){
        mUpdatesCount -= 1;

        if(isSuccess)
            mSuccessCount += 1;

        if(mUpdatesCount == 0){
            forceFinish();
        }
    }

    public synchronized void forceFinish(){
        mListener.SyncFinished(mTotalUpdatesCount, mSuccessCount, type);
    }

    public synchronized void increment(int count){
        mUpdatesCount += count;
        mTotalUpdatesCount += count;
    }

    public synchronized void clear(){
        mUpdatesCount = 0;
        mTotalUpdatesCount = 0;
        mSuccessCount = 0;
    }

}
