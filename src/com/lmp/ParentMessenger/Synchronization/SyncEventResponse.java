package com.lmp.ParentMessenger.Synchronization;

import java.util.List;

public class SyncEventResponse {
    public String id;
    public String action;
    public String created_at;
    public SyncEventTeacherResponse teacher;
    public List<SyncEventPropertyResponse> properties;
}
