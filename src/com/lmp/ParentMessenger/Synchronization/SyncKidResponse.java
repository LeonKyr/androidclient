package com.lmp.ParentMessenger.Synchronization;

public class SyncKidResponse extends SyncKidBase{
    public String id;
    public String code;
    public String hasListeners;
    public SyncImageResponse image;
}
