package com.lmp.ParentMessenger.Synchronization;

import java.util.List;

public class SyncChildWithEventsResponse {
    public SyncKidResponse child;
    public List<SyncEventResponse> events;
}
