package com.lmp.ParentMessenger.Synchronization;

import android.os.AsyncTask;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;

public class SyncTask extends AsyncTask<SyncTaskInput, Void, Boolean> {
    SyncTaskInput input;

    protected Boolean doInBackground(SyncTaskInput... inputs) {
        input = inputs[0];

        try{

            input.increment(1);

            try{
                WebServiceHelper.TrySyncEvents(input);
            }catch(Exception ignored){input.decrement(false);}

            //try{
            //    WebServiceHelper.TrySyncActivities(input);
            //}catch(Exception ignored){input.decrement(false);}

        }
        catch(Exception ex){
            return false;
        }
        finally {
        }

        return true;
    }
}