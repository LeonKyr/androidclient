package com.lmp.ParentMessenger.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.lmp.ParentMessenger.Domain.Kid;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.util.List;

public class ChildrenAdapter extends ArrayAdapter<Kid> {

    private final Context context;
    private final int layoutResourceId;
    private final List<Kid> data;
    private KidHolder holder;
    private LayoutInflater inflater;

    public ChildrenAdapter(Context context, int layoutResourceId, List<Kid> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Kid element = data.get(position);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KidHolder();
            holder.name = (TextView)row.findViewById(R.id.kid_row_name);
            holder.image = (ImageView)row.findViewById(R.id.kid_row_image);
            holder.isSelected = (CheckBox)row.findViewById(R.id.kid_row_isselected);
            row.setTag(holder);
        }else{
            holder = (KidHolder)row.getTag();
        }

        holder.name.setText(element.getName());

        if(element.getImage() != null)
            holder.image.setImageBitmap(ImageHelper.getBitmap(element.getImage().getData()));
        else
            holder.image.setImageResource(R.drawable.pm_default_photo);

        holder.isSelected.setChecked(element.getIsSelected());
        holder.isSelected.setTag(element.getId());

        return row;
    }

    public static class KidHolder {
        TextView name;
        ImageView image;
        public CheckBox isSelected;
    }
}
