package com.lmp.ParentMessenger.Adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.ImageHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<String> _filePaths = new ArrayList<String>();
    private int imageWidth;
    private LayoutInflater inflater;
    private ImageHolder holder;
    AssetManager assetManager;

    public ImageAdapter(Context context, ArrayList<String> filePaths, int imageWidth) {
        this._context = context;
        this._filePaths = filePaths;
        this.imageWidth = imageWidth;
        this.inflater = LayoutInflater.from(context);
        this.assetManager = context.getAssets();
    }

    @Override
    public int getCount() {
        return this._filePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return this._filePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String filePath = _filePaths.get(position);
        ImageView row = null;

        if (convertView == null) {
            row = (ImageView)inflater.inflate(R.layout.activity_eventdetail_photos_photo, parent, false);

            //holder = new ImageHolder();
            //holder.image = (ImageView)row.findViewById(R.id.photo_detail_image);
            //holder.image.setTag(position);
            //row.setTag(holder);
            row.setTag(position);

        } else {
            //holder = (ImageHolder)row.getTag();
            row = (ImageView)convertView;
        }

        // get screen dimensions
        Bitmap image = ImageHelper.decodeFile(filePath, imageWidth);

        /*if(filePath.contains("_asset")){
            try {
                InputStream open = assetManager.open(filePath.replace("assets://",""));
                image = BitmapFactory.decodeStream(open);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            image = ImageHelper.decodeFile(filePath, imageWidth);
        }*/

        row.setScaleType(ImageView.ScaleType.CENTER_CROP);
        row.setLayoutParams(new GridView.LayoutParams(imageWidth, imageWidth));
        row.setImageBitmap(image);

        return row;
    }

    public static class ImageHolder {
        ImageView image;
    }
}