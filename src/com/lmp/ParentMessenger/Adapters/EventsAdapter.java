package com.lmp.ParentMessenger.Adapters;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.lmp.ParentMessenger.Domain.EventDetail;
import com.lmp.ParentMessenger.R;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.lmp.ParentMessenger.Background.EventHelper;
import com.lmp.ParentMessenger.Domain.Event;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Domain.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class EventsAdapter extends ArrayAdapter<Event> {

    private final Context context;
    private final int layoutResourceId;
    private List<Event> data;
    private KidHolder holder;
    private LayoutInflater inflater;
    AssetManager assetManager;

    public Map<String, Bitmap> kidImages;

    public EventsAdapter(Context context, int layoutResourceId, List<Event> data, Map<String, Bitmap> kidImages)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
        this.kidImages = kidImages;
        this.inflater = LayoutInflater.from(context);
        assetManager = context.getAssets();
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Event element = data.get(position);
        String type = element.getType();

        View row = convertView;
        if (row == null || (((KidHolder)row.getTag()).position != position)) {
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new KidHolder();
            holder.position = position;
            holder.eventTime = (TextView)row.findViewById(R.id.event_time);
            holder.kidImage = (ImageView)row.findViewById(R.id.event_kid_image);
            holder.eventTypeImage = (ImageView)row.findViewById(R.id.event_type_image);
            holder.eventBody = (ViewStub)row.findViewById(R.id.event_body);

            if(type.equals(ActionType.Note)){
                holder.eventTypeImage.setImageResource(R.drawable.action_note_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_note);
                View body = holder.eventBody.inflate();

                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_title);
                holder.prop2Text = (TextView)body.findViewById(R.id.event_message);
            }
            else if(type.equals(ActionType.Signin) || type.equals(ActionType.Signout)){
                if(type.equals(ActionType.Signin)){
                    holder.eventTypeImage.setImageResource(R.drawable.action_signin_icon);
                    holder.eventBody.setLayoutResource(R.layout.events_list_row_signin);
                }else{
                    holder.eventTypeImage.setImageResource(R.drawable.action_signout_icon);
                    holder.eventBody.setLayoutResource(R.layout.events_list_row_signout);
                }

                View body = holder.eventBody.inflate();
                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_signtime);
            }
            else if(type.equals(ActionType.Photos)){
                holder.eventTypeImage.setImageResource(R.drawable.action_photos_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_image);
                View body = holder.eventBody.inflate();
                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_message);
            }
            else if(type.equals(ActionType.Sleep)){
                holder.eventTypeImage.setImageResource(R.drawable.action_sleep_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_sleep);
                View body = holder.eventBody.inflate();

                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);

                holder.prop1Text = (TextView)body.findViewById(R.id.event_sleepduration);
                holder.prop2Text = (TextView)body.findViewById(R.id.event_sleepquality);
            }
            else if(type.equals(ActionType.Meal)){
                holder.eventTypeImage.setImageResource(R.drawable.action_meal_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_meal);
                View body = holder.eventBody.inflate();

                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_mealtype);
                holder.prop2Text = (TextView)body.findViewById(R.id.event_mealquality);
            }
            else if(type.equals(ActionType.Absence)){
                holder.eventTypeImage.setImageResource(R.drawable.action_childabsence_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_absence);
                View body = holder.eventBody.inflate();

                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_absenceDesc);
            }
            else if(type.equals(ActionType.WillHaveMeal)){
                holder.eventTypeImage.setImageResource(R.drawable.action_childwillhavemeal_icon);
                holder.eventBody.setLayoutResource(R.layout.events_list_row_willhavemeal);
                View body = holder.eventBody.inflate();

                holder.eventFrom = (TextView)body.findViewById(R.id.event_from);
                holder.prop1Text = (TextView)body.findViewById(R.id.event_willhavemealmessage);
            }
            row.setTag(holder);
        }else{
            holder = (KidHolder)row.getTag();
        }


        if(holder.eventFrom != null)
            holder.eventFrom.setText(element.getTeacherName());

        long milis = element.getCreatedOn() * 1000;

        if(DateUtils.isToday(milis))
            holder.eventTime.setText(EventHelper.DateFormatTodayEventListItem.format(milis));
        else
            holder.eventTime.setText(EventHelper.DateFormatNotTodayEventListItem.format(milis));

        if(kidImages.containsKey(element.getkidExternalId()))
            holder.kidImage.setImageBitmap(kidImages.get(element.getkidExternalId()));
        else
            holder.kidImage.setImageResource(R.drawable.pm_default_photo);

        if(type.equals(ActionType.Note)){
            String title = element.GetDetailValue(ActionKey.Title);
            String message = element.GetDetailValue(ActionKey.Message);

            if(title != null && title.length() > 25)
                title = title.substring(0,22) +"...";

            if(message != null && message.length() > 45)
                message = message.substring(0,42)+"...";

            holder.prop1Text.setText(title);
            holder.prop2Text.setText(message);
        }
        else if(type.equals(ActionType.Signin) || type.equals(ActionType.Signout)){
            String time = element.GetDetailValue(ActionKey.Datetime);
            long timeMilis = 0;
            if(time != null){
                timeMilis = Long.valueOf(time);
                holder.prop1Text.setText(EventHelper.DateFormatIventDetails.format(timeMilis));
            }
        }
        else if(type.equals(ActionType.Photos)){
            /*String message = element.GetDetailValue(ActionKey.Message);

            if(message != null && message.length() > 45)
                message = message.substring(0,42)+"...";
            */
            //String message = "";

            int imageIds[] = new int[]{R.id.photo_image1, R.id.photo_image2, R.id.photo_image3};
            int currId = 0;

            for(EventDetail detail: element.getEventDetails()){
                String key = detail.getKey();
                if(key != null && (key.endsWith("_thumb")/* || key.endsWith("_asset")*/)){
                    if(currId < imageIds.length){
                        ImageView im = (ImageView)row.findViewById(imageIds[currId]);

                        if(key.endsWith("_thumb")){
                            im.setImageBitmap(ImageHelper.decodeFile(detail.getValue(), 20));
                        }
                        /*else if(key.endsWith("_asset")){
                            try {
                                InputStream open = assetManager.open(detail.getValue().replace("assets://",""));
                                Bitmap bitmap = BitmapFactory.decodeStream(open);
                                im.setImageBitmap(bitmap);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }*/
                    }
                    currId ++;
                }
            }

            String newImagesText = context.getResources().getString(R.string.NewImagesText);
            holder.prop1Text.setText(newImagesText + " ("+currId+")");
        }
        else if(type.equals(ActionType.Sleep)){
            String duration = getStringResourceByName("sleepDuration"+element.GetDetailValue(ActionKey.SleepDuration));
            String quality = getStringResourceByName("sleepQuality"+element.GetDetailValue(ActionKey.SleepQuality));

            //String message = element.GetDetailValue(ActionKey.Message);

            holder.prop1Text.setText(duration);
            holder.prop2Text.setText(quality);
        }
        else if(type.equals(ActionType.Meal)){
            String mealtype = getStringResourceByName("mealType"+element.GetDetailValue(ActionKey.MealType));
            String quality = getStringResourceByName("mealQuality"+element.GetDetailValue(ActionKey.MealQuality));

            //String message = element.GetDetailValue(ActionKey.Message);

            holder.prop1Text.setText(mealtype);
            holder.prop2Text.setText(quality);
        }
        else if(type.equals(ActionType.Absence)){
            String template = getStringResourceByName("kidMarkedAbsent");
            String reason = getStringResourceByName("absenceReason"+element.GetDetailValue(ActionKey.AbsenceReason));

            String desc = String.format(template, reason);
            holder.prop1Text.setText(desc);
        }
        else if(type.equals(ActionType.WillHaveMeal)){
            String template = getStringResourceByName("kidWillHaveMeal");
            String mealtype = getStringResourceByName("mealType"+element.GetDetailValue(ActionKey.MealType));

            String desc = String.format(template, mealtype);
            holder.prop1Text.setText(mealtype);
        }
        return row;
    }

    private String getStringResourceByName(String aString) {
        // TODO: remove after fix
        if(aString.contains(".")){
            String[] parts = aString.split("\\.");
            aString = parts[0];
        }

        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }

    public static class KidHolder {
        int position;
        TextView eventTime;
        ImageView kidImage;
        ImageView eventTypeImage;
        ViewStub eventBody;
        TextView eventFrom;

        TextView prop1Text;
        TextView prop2Text;
        TextView prop3Text;
    }

    public void swapItems(List<Event> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
