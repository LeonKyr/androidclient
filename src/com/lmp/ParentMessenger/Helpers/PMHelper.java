package com.lmp.ParentMessenger.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.kodart.httpzoid.ResponseHandler;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Locale;

public class PMHelper
{
    public static final String SCHEDULED_ACTIVITY_FORMAT = "dd MMM yy";
    public static String sharedPrefsName = "com.babify.prefs";

    public static String deviceIdPrefKey = "deviceId";
    public static boolean istutorialAfterLoginAlreadyShown = false;
    public static int tutorialAfterLoginShowCount = 1;
    public static String redirectToExtra = "redirectTo";
    public static String tutorialImages = "tutorialImages";

    public static String[] SupportedLanguages = new String[]{"en","ru","de","es","ca"};

    /*public static int[] tutorialFirstTimeLayoutsArray =
            {
                    R.drawable.pm_tutorial_improve_comminication_and_satisfaction,
                    R.drawable.pm_tutorial_increase_revenue,
                    R.drawable.pm_tutorial_save_memories
            };*/

    public static int[] tutorialAfterLoginImagesArray =
            {
                    R.drawable.pm_tutorial_improve_comminication_and_satisfaction,
                    R.drawable.pm_tutorial_increase_revenue,
                    R.drawable.pm_tutorial_save_memories
            };

    public static String ServiceUrl = "http://api.parentmgr.com/v1/";
    public static String SyncFinishedBroadcastAction = "com.babify.AndroidClientParent.SyncFinished";
    public static String SyncPushBroadcastAction = "com.babify.AndroidClientParent.PushReceived";

    public static void validationFailedRule(Context context, View failedView, Rule<?> failedRule){
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            UIHelper.showToast(context, message, Toast.LENGTH_SHORT, UIHelper.TDError);
        }

    }

    public static SharedPreferences getPreferences(Context context, String name) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static String getDefaultKidCode() {
        String language = Locale.getDefault().getLanguage();
        String prefix = "default-";
        if(Arrays.asList(SupportedLanguages).contains(language)){
            return prefix + language;
        }else{
            return prefix + "en";
        }
    }
}