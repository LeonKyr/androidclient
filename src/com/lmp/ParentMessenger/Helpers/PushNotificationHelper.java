package com.lmp.ParentMessenger.Helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Synchronization.SyncListenerRequest;
import com.lmp.ParentMessenger.Synchronization.SyncListenerResponse;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.io.IOException;

public class PushNotificationHelper {
    private static final String SENDER_ID = "250362718665";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static void registerForPushNotifications(Activity activity){
        GoogleCloudMessaging gcm;
        String regid;

        if (checkPlayServices(activity)) {
            gcm = GoogleCloudMessaging.getInstance(activity);
            regid = getRegistrationId(activity, PMHelper.sharedPrefsName);

            if (regid == null || regid.isEmpty()) {
               register(activity, gcm);
            }
            //else{
            //    UIHelper.showToast(activity, "Can't subscribe to Push Notifications", Toast.LENGTH_LONG, UIHelper.TDError);
            //}
        } else {
            UIHelper.showToast(activity, "Can't subscribe to Push Notifications", Toast.LENGTH_LONG, UIHelper.TDError);
        }

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private static boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                return false;
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private static String getRegistrationId(Context context, String prefName) {
        final SharedPreferences prefs = PMHelper.getPreferences(context, prefName);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }
    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private static void storeRegistrationId(Context context, String regId, String prefName) {
        final SharedPreferences prefs = PMHelper.getPreferences(context, prefName);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private static void register(final Context context, final GoogleCloudMessaging gcm) {
        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";

                try {
                    String regid = gcm.register(SENDER_ID);

                    // catch regId in GcmBroadcastReceiver

                    //if(regid != null && !regid.equals(""))
                    //    registerInBackground(context, prefName, regid);
                    //}
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    //UIHelper.showToast(context, "Can't extract token for Push Notifications", Toast.LENGTH_LONG, UIHelper.TDError);
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }

                return msg;
            }
        };

        task.execute();
    }

    public static void registerInBackground(final Context context, final String regid) {
            Http http = HttpFactory.create(context);
            final SyncListenerRequest request = new SyncListenerRequest();

            DatabaseHelper dbHelper = new DatabaseHelper(context);

            String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);

            String url = PMHelper.ServiceUrl+String.format("push/%s/platform/droid/with-device-token/%s/true/",deviceId,regid);
            http.post(url)
                    .data(request)
                    .handler(new ResponseHandler<SyncListenerResponse>(){
                        @Override
                        public void success(SyncListenerResponse response, HttpResponse httpResponse) {
                            storeRegistrationId(context, regid, PMHelper.sharedPrefsName);
                        }

                        @Override
                        public void error(String message, HttpResponse response) {
                        }

                        @Override
                        public void failure(NetworkError error) {
                        }

                        @Override
                        public void complete() {
                        }
                    }).send();
    }
}
