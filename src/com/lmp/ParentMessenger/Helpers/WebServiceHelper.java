package com.lmp.ParentMessenger.Helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Activities.PMApplication;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Event;
import com.lmp.ParentMessenger.Domain.Kid;
import com.lmp.ParentMessenger.Domain.Media;
import com.lmp.ParentMessenger.Domain.MediaType;
import com.lmp.ParentMessenger.Synchronization.*;
import com.mbalychev.Shared.Activities.ImagePickerActivity;
import com.mbalychev.Shared.Domain.ActionType;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;

public class WebServiceHelper
{
    public static SyncDevice GetDevice(String deviceId) {
        SyncDevice device = new SyncDevice();
        device.id =  deviceId;

        return device;
    }

    public static void TrySyncActivities(final SyncTaskInput input) {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        long lastUpdated = dbHelper.getLastActivityDate();
        if(lastUpdated > 0)
            lastUpdated += 1;

        String url = PMHelper.ServiceUrl+"activities/"+input.listenerExternalId+"/"+lastUpdated+"/20/";

        Cancellable canc = http.get(url)
                //.data(request)
                .handler(new ResponseHandler<Object>() {
                    @Override
                    public void success(final Object response, HttpResponse httpResponse) {
                        input.decrement(true);
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }

                    @Override
                    public void complete() {
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TrySyncEvents(final SyncTaskInput input) {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        long lastUpdated = dbHelper.getLastEventDate();
        if(lastUpdated > 0)
            lastUpdated += 1;

        String url = PMHelper.ServiceUrl+"events/"+input.listenerExternalId+"/"+lastUpdated+"/20/";

        final int[] responseCounts = {0,0};

        Cancellable canc = http.get(url)
                //.data(request)
                .handler(new ResponseHandler<SyncChildWithEventsResponse[]>() {
                    @Override
                    public void success(final SyncChildWithEventsResponse[] response, HttpResponse httpResponse) {
                        if (response != null && response.length > 0)
                            for (final SyncChildWithEventsResponse childWithEvent : response) {
                                if (childWithEvent.child != null) {
                                    Kid kid = dbHelper.getKid(0, childWithEvent.child.id, null);
                                    if (kid == null) {
                                        long dobL = Long.valueOf(childWithEvent.child.dob);
                                        String imageId = null;
                                        byte[] image = null;

                                        dbHelper.addKid(input.listenerExternalId, childWithEvent.child.id, childWithEvent.child.name, dobL, imageId, image, childWithEvent.child.code);
                                        responseCounts[1]++;
                                        //kid = dbHelper.getKid(0, childWithEvent.child.id, null);
                                    }
                                    if (childWithEvent.child.image != null) {
                                        Media media = dbHelper.getMediaByExternalId(childWithEvent.child.image.id);
                                        if(media == null){
                                            Http http2 = HttpFactory.create(input.context);
                                            input.increment(1);
                                            Cancellable canc2 = http2.get(PMHelper.ServiceUrl + "image/" + childWithEvent.child.image.id)
                                                    .handler(new ResponseHandler<InputStream>() {
                                                        @Override
                                                        public void success(InputStream imageresponse, HttpResponse httpResponse) {
                                                            try {
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                Bitmap bm = BitmapFactory.decodeStream(imageresponse, null, options);
                                                                byte[] bytes = ImageHelper.getBytes(bm);

                                                                long imageid = dbHelper.addMedia(childWithEvent.child.image.id, MediaType.image.toString(), "", bytes);

                                                                if (imageid > 0)
                                                                    dbHelper.updateMediaId(DatabaseHelper.KIDS_TABLE_NAME, childWithEvent.child.id, imageid);

                                                            } catch (Exception ex) {
                                                                UIHelper.showToast(input.context, "Unable to fetch image", Toast.LENGTH_LONG, UIHelper.TDError);
                                                            }finally{
                                                                input.decrement(true);
                                                            }
                                                        }

                                                        @Override
                                                        public void error(String message, HttpResponse response) {
                                                            input.decrement(false);
                                                        }

                                                        @Override
                                                        public void failure(NetworkError error) {
                                                            input.decrement(false);
                                                        }
                                                    }).send();

                                            if(canc2 == Cancellable.Empty)
                                                input.decrement(false);
                                        }
                                    }

                                    if (childWithEvent.events != null && childWithEvent.events.size() > 0) {
                                        for (SyncEventResponse event : childWithEvent.events) {
                                            Event exEvent = dbHelper.getEventByExternalId(event.id);
                                            if(exEvent != null)
                                                continue;

                                            dbHelper.addEvent(input.listenerExternalId, childWithEvent.child.id, event);
                                            responseCounts[0]++;

                                            if (event.action.equals(ActionType.Photos)) {
                                                for (SyncEventPropertyResponse property : event.properties) {
                                                    if (property.type.name.equals(ActionType.Photos)) {
                                                        if (property.value instanceof ArrayList) {
                                                            ArrayList photosDetailsList = (ArrayList) property.value;
                                                            if (photosDetailsList != null && photosDetailsList.size() > 0) {
                                                                for (Object detail : photosDetailsList) {
                                                                    if (detail instanceof Map) {
                                                                        Map photosDetailsList2 = (Map) detail;
                                                                        if (photosDetailsList2.size() == 1 && photosDetailsList2.containsKey("id")) {
                                                                            String value = photosDetailsList2.get("id").toString();
                                                                            TryGetImage(input, event.id, value, true);
                                                                            input.increment(1);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        input.decrement(true);
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }

                    @Override
                    public void complete() {
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TryGetImage(final SyncTaskInput input, final String eventExternalId, final String externalImageId, final boolean isThumb) {
        Http http = HttpFactory.create(input.context);
        final DatabaseHelper dbHelper = new DatabaseHelper(input.context);

        Cancellable canc = http.get(PMHelper.ServiceUrl + (isThumb ? "thumb" : "image") + "/"+externalImageId)
                .handler(new ResponseHandler<InputStream>() {
                    @Override
                    public void success(InputStream inputStream, HttpResponse httpResponse) {
                        try {
                            final File file = ImagePickerActivity.getPhotoFile(input.context);
                            if (file != null) {
                                final OutputStream output = new FileOutputStream(file);
                                try {
                                    try {
                                        final byte[] buffer = new byte[1024];
                                        int read;

                                        while ((read = inputStream.read(buffer)) != -1)
                                            output.write(buffer, 0, read);

                                        output.flush();

                                        String filePath = file.getAbsolutePath();
                                        dbHelper.addEventDetail(eventExternalId, externalImageId.toString()+ (isThumb ? "_thumb" : "_full"), filePath);
                                    } finally {
                                        output.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (FileNotFoundException e) {

                        } finally {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                            }
                        }

                        input.decrement(true);
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        input.decrement(false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        input.decrement(false);
                    }

                    @Override
                    public void complete() {
                    }
                }).send();

        if(canc == Cancellable.Empty)
            input.decrement(false);
    }

    public static void TryRegisterKid(final Activity activity, final String uniqueCode, final String listenerId, final ProgressDialog dialog, final Boolean isRegistration){

        final DatabaseHelper dbHelper = new DatabaseHelper(activity.getApplicationContext());

        Http http = HttpFactory.create(activity.getApplicationContext());
        String url = PMHelper.ServiceUrl+"listener/"+listenerId+"/add-child/"+uniqueCode+"/";
        Cancellable canc = http.post(url)
                .data(null)
                .handler(new ResponseHandler<SyncKidResponse>() {
                    @Override
                    public void success(final SyncKidResponse response, HttpResponse httpResponse) {
                        dbHelper.addKid(listenerId, response.id, response.name, Long.valueOf(response.dob), null, null, response.code);

                        if (response.image != null) {
                            Http http = HttpFactory.create(activity.getApplicationContext());
                            http.get(PMHelper.ServiceUrl + "image/" + response.image.id)
                                    .handler(new ResponseHandler<InputStream>() {
                                        @Override
                                        public void success(InputStream imageresponse, HttpResponse httpResponse) {
                                            try {
                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                Bitmap bm = BitmapFactory.decodeStream(imageresponse, null, options);
                                                byte[] bytes = ImageHelper.getBytes(bm);

                                                long imageid = dbHelper.addMedia(response.image.id, MediaType.image.toString(), "", bytes);

                                                if (imageid > 0)
                                                    dbHelper.updateMediaId(DatabaseHelper.KIDS_TABLE_NAME, response.id, imageid);

                                            } catch (Exception ex) {
                                                UIHelper.showToast(activity.getApplicationContext(), "Unable to fetch a photo", Toast.LENGTH_LONG, UIHelper.TDError);
                                            } finally {
                                                sendSyncFinishedBroadcastAndRunSync(activity, listenerId);
                                                if (dialog != null)
                                                    activity.finish();
                                            }
                                        }

                                        @Override
                                        public void error(String message, HttpResponse response) {
                                        }

                                        @Override
                                        public void failure(NetworkError error) {
                                        }

                                        @Override
                                        public void complete() {
                                            if (dialog != null)
                                                dialog.dismiss();
                                        }
                                    }).send();
                        } else {
                            sendSyncFinishedBroadcastAndRunSync(activity,listenerId);

                            if (dialog != null) {
                                dialog.dismiss();
                                activity.finish();
                            }
                        }
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                        UIHelper.showToast(activity.getApplicationContext(), activity.getString(com.lmp.ParentMessenger.R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);

                        if (dialog != null)
                            dialog.dismiss();

                        if(isRegistration)
                            TryRegisterKid(activity, PMHelper.getDefaultKidCode(), listenerId, null, false);
                    }

                    @Override
                    public void failure(NetworkError error) {
                        UIHelper.showToast(activity.getApplicationContext(), activity.getString(com.lmp.ParentMessenger.R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);

                        if (dialog != null)
                            dialog.dismiss();

                        if(isRegistration)
                            TryRegisterKid(activity, PMHelper.getDefaultKidCode(), listenerId, null, false);
                    }

                    @Override
                    public void complete() {
                    }
                }).send();
    }

    public static void sendSyncFinishedBroadcastAndRunSync(Activity activity, String listenerId){
        Intent broadcast = new Intent();
        broadcast.putExtra("total", 1);
        broadcast.putExtra("success", 1);
        broadcast.setAction(PMHelper.SyncFinishedBroadcastAction);
        activity.sendBroadcast(broadcast);

        ((PMApplication)activity.getApplicationContext()).runSyncTask(listenerId);
    }

    public static void TryThankYou(final Activity activity, final String listenerId, final String eventId){

        Http http = HttpFactory.create(activity.getApplicationContext());
        String url = PMHelper.ServiceUrl+"thank-you/listener/"+listenerId+"/"+eventId+"/";
        http.post(url)
                .handler(new ResponseHandler<Object>() {
                    @Override
                    public void success(final Object response, HttpResponse httpResponse) {
                    }

                    @Override
                    public void error(String message, HttpResponse response) {
                    }

                    @Override
                    public void failure(NetworkError error) {
                    }

                    @Override
                    public void complete() {
                    }
                }).send();
    }
}