package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Kid;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class AddChildActivity extends PMActivity implements Validator.ValidationListener{
    @Required(order = 1)
    EditText uniqueCodeText;

    DatabaseHelper dbHelper;
    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_addchild);

        dbHelper = new DatabaseHelper(getApplicationContext());

        uniqueCodeText = (EditText)findViewById(R.id.addchild_uniquecode);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.addChild));
    }

    @Override
    public void onValidationSucceeded() {
        final String uniqueCode = uniqueCodeText.getText().toString();

        Kid kid = dbHelper.getKid(0, null, uniqueCode);
        if(kid != null)
            UIHelper.showToast(getApplicationContext(), getString(R.string.kidAlreadyRegistered), Toast.LENGTH_LONG, UIHelper.TDError);
        else{
            try{
                if(UIHelper.hasNetwork(getApplicationContext()))
                {
                    final ProgressDialog dialog  = new ProgressDialog(this);
                    dialog.setMessage(getString(R.string.pleaseWait));
                    dialog.show();

                    WebServiceHelper.TryRegisterKid(AddChildActivity.this, uniqueCode, getCurrentListenerId(), dialog, false);
                }else{
                    UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        PMHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_saveonly, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }
}