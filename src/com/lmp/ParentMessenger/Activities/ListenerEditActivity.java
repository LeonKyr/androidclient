package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Domain.Media;
import com.lmp.ParentMessenger.Domain.MediaType;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.lmp.ParentMessenger.Synchronization.SyncImageResponse;
import com.lmp.ParentMessenger.Synchronization.SyncListenerRequest;
import com.lmp.ParentMessenger.Synchronization.SyncListenerResponse;
import com.mbalychev.Shared.Activities.ImagePickerActivity;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ListenerEditActivity extends PMActivity  implements Validator.ValidationListener{
    @Required(order = 1)
    EditText name;

    EditText email;

    ImageView pictureImageView;
    Bitmap bitmap = null;

    CheckBox isAutoLogin;
    //Button changePassword;

    DatabaseHelper dbHelper;
    Validator validator;
    private final int PICK_BITMAP = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listener_info_edit);
        //super.onCreate(savedInstanceState, R.layout.listener_info_edit, R.layout.left_slider_menu_full, R.string.profile);

        dbHelper = new DatabaseHelper(getApplicationContext());

        name = (EditText)findViewById(R.id.listener_name);
        email = (EditText)findViewById(R.id.listener_email);
        pictureImageView = (ImageView)findViewById(R.id.listener_picture);
        isAutoLogin = (CheckBox)findViewById(R.id.listener_is_autologin);
        //changePassword = (Button)findViewById(R.id.listener_change_pwd_btn);

        Listener listener = dbHelper.getListener(getCurrentListenerId());

        name.setText(listener.getName());
        email.setText(listener.getEmail());
        isAutoLogin.setChecked(listener.getIsAutoLogin());

        if(listener.getImage() != null){
            bitmap = ImageHelper.getBitmap(listener.getImage().getData());
            pictureImageView.setImageBitmap(bitmap);
        }

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.profile));
        bar.setSubtitle(getString(R.string.updateInfo));

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_saveonly, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        byte[] imageBytes = null;
        if(bitmap != null)
            imageBytes = ImageHelper.getBytes(bitmap);
        final byte[] finalImageBytes = imageBytes;

        final String newName = name.getText().toString();

        if(UIHelper.hasNetwork(getApplicationContext()))
        {
            final ProgressDialog dialog  = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.pleaseWait));
            dialog.show();

            final Listener current = getCurrentListener();

            Media currentMedia = current.getImage();
            long imageId = 0;
            if(currentMedia == null){
                if(finalImageBytes != null)
                    imageId = dbHelper.addMedia(null, MediaType.image.toString(),"",finalImageBytes);
            }else{
                if(finalImageBytes != null)
                    imageId = currentMedia.getId();
            }

            final long finalImageId = imageId;

            if(imageId > 0){
                Http http = HttpFactory.create(getApplicationContext());

                final InputStream is = new ByteArrayInputStream(finalImageBytes);
                String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);
                String path = PMHelper.ServiceUrl + "image/" + WebServiceHelper.GetDevice(deviceId).id;
                if(currentMedia != null && currentMedia.getExternalId() != null && !currentMedia.getExternalId().isEmpty()){
                    path += "/"+currentMedia.getExternalId();
                }

                http.post(path)
                        .data(is)
                        .handler(new ResponseHandler<SyncImageResponse>() {
                            @Override
                            public void success(SyncImageResponse response, HttpResponse httpResponse) {
                                dbHelper.updateMedia(finalImageId, response.id,"",finalImageBytes);
                            }

                            @Override
                            public void error(String message, HttpResponse response) {
                            }

                            @Override
                            public void failure(NetworkError error) {
                            }

                            @Override
                            public void complete()
                            {
                                try {
                                    is.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                dialog.dismiss();
                            }
                        }).send();
            }

            final SyncListenerRequest request = new SyncListenerRequest();
            request.email = current.getEmail();
            request.name = newName;
            request.password =  current.getPassword();

            String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);
            request.device = WebServiceHelper.GetDevice(deviceId);

            Http http = HttpFactory.create(getApplicationContext());
            http.post(PMHelper.ServiceUrl+"listener/"+getCurrentListenerId())
                    .data(request)
                    .handler(new ResponseHandler<SyncListenerResponse>() {
                        @Override
                        public void success(SyncListenerResponse response, HttpResponse httpResponse) {

                            dbHelper.updateListener(getCurrentListenerId(), newName, current.getEmail(), isAutoLogin.isChecked(), finalImageId);
                            setCurrentListener(dbHelper.getListener(getCurrentListenerId()));
                            finish();
                        }

                        @Override
                        public void error(String message, HttpResponse response) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void failure(NetworkError error) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void complete() {
                            dialog.dismiss();
                        }
                    }).send();
        }else{
            UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        PMHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    public void makePicture(View v){
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        startActivityForResult(intent, PICK_BITMAP);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_BITMAP){
            byte[] bytes = data.getByteArrayExtra("bitmap_bytes");
            if(bytes != null)
            {
                bitmap = ImageHelper.getBitmap(bytes);
                pictureImageView.setImageBitmap(bitmap);
            }
        }
    }
    public void changePwdClick(View v){
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }
}