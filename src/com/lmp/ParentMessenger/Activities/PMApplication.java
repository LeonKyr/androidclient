package com.lmp.ParentMessenger.Activities;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.Synchronization.ISyncFinishedListener;
import com.lmp.ParentMessenger.Synchronization.SyncTask;
import com.lmp.ParentMessenger.Synchronization.SyncTaskInput;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class PMApplication extends Application  implements ISyncFinishedListener {
    private Listener CurrentListener;
    private SyncTask syncTask;
    SyncTaskInput input;
    DatabaseHelper dbHelper;

    public Listener getCurrentListener() {
        return CurrentListener;
    }

    public void setCurrentListener(Listener currentListener) {
        CurrentListener = currentListener;
    }

    public boolean isSynchnonizing(){
        return input.isSynchnonizing;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        input = new SyncTaskInput(this);
        input.isSynchnonizing = false;

        dbHelper = new DatabaseHelper(getApplicationContext());
        String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, UIHelper.getDeviceUniqueId());

        initImageLoader(getApplicationContext());
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public void runSyncTask(final String listenerId){
        runSyncTask(listenerId, SyncTaskInput.GeneralType);
    }

    private void runSyncTask(final String listenerId, String type){
        if(!input.isSynchnonizing){
            input.isSynchnonizing = true;
            if(UIHelper.hasNetwork(getApplicationContext()))
            {
                input.clear();

                input.context = getApplicationContext();
                input.listenerExternalId = listenerId;
                input.type = type;

                syncTask = new SyncTask();
                syncTask.execute(input);
            }else{
                input.isSynchnonizing = false;
            }
        }
    }

    @Override
    public void SyncFinished(int totalCount, int successCount, String type) {
        input.isSynchnonizing = false;

        Intent broadcast = new Intent();
        broadcast.putExtra("total",totalCount);
        broadcast.putExtra("success",successCount);
        broadcast.setAction(PMHelper.SyncFinishedBroadcastAction);
        sendBroadcast(broadcast);

    }
}