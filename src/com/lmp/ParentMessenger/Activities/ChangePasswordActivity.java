package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.lmp.ParentMessenger.Synchronization.SyncListenerRequest;
import com.lmp.ParentMessenger.Synchronization.SyncListenerResponse;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

public class ChangePasswordActivity extends PMActivity implements Validator.ValidationListener{
    @Required(order = 1)
    @TextRule(order = 2, minLength = 6, message = "Enter at least 6 characters.")
    EditText oldPwdText;

    @Required(order = 3)
    @TextRule(order = 4, minLength = 6, message = "Enter at least 6 characters.")
    EditText newPwdText;

    DatabaseHelper dbHelper;
    Validator validator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //PMHelper.showAsPopup(this);
        setContentView(R.layout.activity_changepassword);

        dbHelper = new DatabaseHelper(getApplicationContext());

        oldPwdText = (EditText)findViewById(R.id.changepwd_oldpwd);
        newPwdText = (EditText)findViewById(R.id.changepwd_newpwd);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.changePassword));
    }

    @Override
    public void onValidationSucceeded() {
        final String oldPwd = oldPwdText.getText().toString();
        final String newPwd = newPwdText.getText().toString();

        try{
            final String hashOldPassword = AeSimpleSHA1.MD5(oldPwd);
            final String hashNewPassword = AeSimpleSHA1.MD5(newPwd);

            Listener listener = getCurrentListener();
            if(!listener.getPassword().equals(hashOldPassword))
                UIHelper.showToast(getApplicationContext(), getString(R.string.oldPasswordIsWrong), Toast.LENGTH_LONG, UIHelper.TDError);
            else{
                if(UIHelper.hasNetwork(getApplicationContext()))
                {
                    final ProgressDialog dialog  = new ProgressDialog(this);
                    dialog.setMessage(getString(R.string.pleaseWait));
                    dialog.show();

                    Listener current = getCurrentListener();

                    final SyncListenerRequest request = new SyncListenerRequest();
                    request.email = current.getEmail();
                    request.name = current.getName();
                    request.password =  hashNewPassword;

                    String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);
                    request.device = WebServiceHelper.GetDevice(deviceId);

                    Http http = HttpFactory.create(getApplicationContext());
                    http.post(PMHelper.ServiceUrl+"listener/"+getCurrentListenerId()+"/")
                            .data(request)
                            .handler(new ResponseHandler<SyncListenerResponse>(){
                                @Override
                                public void success(SyncListenerResponse response, HttpResponse httpResponse) {

                                    dbHelper.updateListenerPassword(getCurrentListenerId(), hashNewPassword);
                                    Listener newListener = dbHelper.getListener(getCurrentListenerId());
                                    ((PMApplication)getApplicationContext()).setCurrentListener(newListener);
                                    finish();
                                }

                                @Override
                                public void error(String message, HttpResponse response) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void failure(NetworkError error) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void complete() {
                                    dialog.dismiss();
                                }
                            }).send();
                }else{
                    UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        PMHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_saveonly, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                save();
                break;
            default:
                break;
        }

        return true;
    }

    public void save(){
        validator.validate();
    }
}