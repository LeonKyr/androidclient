package com.lmp.ParentMessenger.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.lmp.ParentMessenger.R;

public class TutorialFragment extends Fragment {
    private final int imageId;

    public TutorialFragment(int imageId){
        this.imageId = imageId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tutorial_page, container, false);
        ImageView image = (ImageView)view.findViewById(R.id.tutorial_image);
        image.setImageResource(imageId);
        return view;
    }
}