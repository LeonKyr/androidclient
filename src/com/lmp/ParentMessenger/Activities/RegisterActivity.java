package com.lmp.ParentMessenger.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.lmp.ParentMessenger.Synchronization.*;
import com.mbalychev.Shared.Domain.ActionKey;
import com.mbalychev.Shared.Domain.ActionType;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

import java.util.ArrayList;
import java.util.UUID;

public class RegisterActivity extends Activity implements Validator.ValidationListener {

    @Required(order = 1)
    EditText nameEditText;

    @Required(order = 2)
    @Email(order = 3, message = "Enter valid email")
    EditText emailEditText;

    @Required(order = 4)
    @TextRule(order = 5, minLength = 6, message = "Enter at least 6 characters.")
    EditText passwordEditText;

    @Checked(order = 6, message = "You must agree to the terms.")
    CheckBox acceptConditionsCheckBox;

    CheckBox isAutoLoginCheckBox;

    EditText childUnuqueCode;

    DatabaseHelper dbHelper;

    Validator validator;
    ProgressDialog dialog;

    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.activity_register);

        dbHelper = new DatabaseHelper(getApplicationContext());

        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);
        Button btnRegister = (Button)findViewById(R.id.btnRegister);

        nameEditText = (EditText)findViewById(R.id.register_name);
        emailEditText = (EditText)findViewById(R.id.register_email);
        passwordEditText = (EditText)findViewById(R.id.register_password);
        isAutoLoginCheckBox = (CheckBox)findViewById(R.id.register_is_autologin);
        acceptConditionsCheckBox = (CheckBox)findViewById(R.id.register_accept_conditions);
        childUnuqueCode = (EditText)findViewById(R.id.addchild_uniquecode);

        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0)
            {
                validator.validate();
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);


        dialog  = new ProgressDialog(RegisterActivity.this);

        // TODO: remove after testing
        //nameEditText.setText("Test name");
        //emailEditText.setText("test1@test.com");
        //passwordEditText.setText("111111");
    }

    @Override
    public void onValidationSucceeded() {
        final String name = nameEditText.getText().toString();
        final String email = emailEditText.getText().toString();
        final String password = passwordEditText.getText().toString();
        final String uniqueCode = childUnuqueCode.getText().toString();

        try
        {
            final String hashPassword = AeSimpleSHA1.MD5(password);

            Listener listener = dbHelper.getListener(email, password);
            if(listener == null)
            {
                if(UIHelper.hasNetwork(getApplicationContext()))
                {
                    dialog.setMessage(getString(R.string.pleaseWait));
                    dialog.show();

                    final SyncListenerRequest request = new SyncListenerRequest();
                    request.email = email;
                    request.name = name;
                    request.password = hashPassword;

                    String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);
                    request.device = WebServiceHelper.GetDevice(deviceId);
                    //request.image = null;

                    Http http = HttpFactory.create(getApplicationContext());
                    http.post(PMHelper.ServiceUrl+"listener")
                            .data(request)
                            .handler(new ResponseHandler<SyncListenerResponse>(){
                                @Override
                                public void success(SyncListenerResponse response, HttpResponse httpResponse) {
                                    long id = dbHelper.addListener(response.id, request.name, request.email, request.password,isAutoLoginCheckBox.isChecked(), null);

                                    ((PMApplication) getApplicationContext()).setCurrentListener(dbHelper.getListener(response.id));

                                    //PMHelper.showToast(getApplicationContext(), getString(R.string.welcome), Toast.LENGTH_LONG, PMHelper.TDSuccess);
                                    startActivity(new Intent(RegisterActivity.this, EventsActivity.class));

                                    if(uniqueCode != null && !uniqueCode.isEmpty()){
                                        WebServiceHelper.TryRegisterKid(RegisterActivity.this, uniqueCode, response.id, null, true);
                                    }else{
                                        WebServiceHelper.TryRegisterKid(RegisterActivity.this, PMHelper.getDefaultKidCode(), response.id, null, false);
                                    }
                                }

                                @Override
                                public void error(String message, HttpResponse response) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void failure(NetworkError error) {
                                    UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                                }

                                @Override
                                public void complete() {
                                    dialog.dismiss();
                                }
                            }).send();
                }else{
                    UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
                }
            }else
            {
                UIHelper.showToast(getApplicationContext(), getString(R.string.register_accountAlreadyExists), Toast.LENGTH_SHORT, UIHelper.TDError);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        PMHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Listener activeListener = dbHelper.getActiveListener();
        if(activeListener != null){
            if(activeListener.getIsAutoLogin()){
                ((PMApplication)getApplicationContext()).setCurrentListener(activeListener);
                startActivity(new Intent(RegisterActivity.this, EventsActivity.class));

                ((PMApplication)getApplicationContext()).runSyncTask(activeListener.getExternalListenerId());
            }
        }
    }

    /*private void AddDemoChild(String listenerId) {
        try{
            String externalId = java.util.UUID.randomUUID().toString();
            UUID imageExternalId = java.util.UUID.randomUUID();

            byte[] image = ImageHelper.getBytes(
                    BitmapFactory.decodeResource(getResources(), R.drawable.demo_kid_photo));

            dbHelper.addKid(
                    listenerId,
                    externalId,
                    UIHelper.getStringResourceByName(this, "demoKidName"), // name
                    1294268400000L, // birthday
                    imageExternalId.toString(),
                    image,
                    "11111111" // unique code
            );

            SyncEventTeacherResponse teacher = new SyncEventTeacherResponse();
            teacher.id = java.util.UUID.randomUUID().toString();
            teacher.name = UIHelper.getStringResourceByName(this, "demoKidTeacherName");

            ArrayList<SyncEventPropertyResponse> properties = new ArrayList<SyncEventPropertyResponse>();

            UUID eventId = null;
            // 1 SignIn
            eventId = java.util.UUID.randomUUID();

            SyncEventResponse event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Signin;
            event.created_at = "1395995416"; // Fri, 28 Mar 2014 08:30:16 GMT
            event.teacher = teacher;

            SyncEventPropertyResponse prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.Datetime;
            prop1.value = "1395995416";
            properties.add(prop1);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 2 Will have meal (lunch)
            eventId = java.util.UUID.randomUUID();
            properties.clear();

            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.WillHaveMeal;
            event.created_at = "1395996350"; //  Fri, 28 Mar 2014 08:45:50 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.MealType;
            prop1.value = "3"; // lunch
            properties.add(prop1);

            SyncEventPropertyResponse prop2 = new SyncEventPropertyResponse();
            prop2.type = new SyncEventPropertyTypeResponse();
            prop2.type.name = ActionKey.Message;
            prop2.value = UIHelper.getStringResourceByName(this, "demoKidEventWillHaveMealMessage");
            properties.add(prop2);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 3 Meal Breakfast
            eventId = java.util.UUID.randomUUID();
            properties.clear();

            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Meal;
            event.created_at = "1395998112"; //   Fri, 28 Mar 2014 09:15:12 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.MealType;
            prop1.value = "1"; // breakfast
            properties.add(prop1);

            prop2 = new SyncEventPropertyResponse();
            prop2.type = new SyncEventPropertyTypeResponse();
            prop2.type.name = ActionKey.MealQuality;
            prop2.value = "2"; // Ate most of the meal
            properties.add(prop2);

            SyncEventPropertyResponse prop3 = new SyncEventPropertyResponse();
            prop3.type = new SyncEventPropertyTypeResponse();
            prop3.type.name = ActionKey.Message;
            prop3.value = UIHelper.getStringResourceByName(this, "demoKidEventMealBreakfastMessage");
            properties.add(prop3);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 4 Note
            eventId = java.util.UUID.randomUUID();
            properties.clear();

            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Note;
            event.created_at = "1396005825"; //  Fri, 28 Mar 2014 11:23:45 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.Title;
            prop1.value = UIHelper.getStringResourceByName(this, "demoKidEventNoteTitle");
            properties.add(prop1);

            prop2 = new SyncEventPropertyResponse();
            prop2.type = new SyncEventPropertyTypeResponse();
            prop2.type.name = ActionKey.Message;
            prop2.value = UIHelper.getStringResourceByName(this, "demoKidEventNoteMessage");
            properties.add(prop2);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 5 Sleep
            eventId = java.util.UUID.randomUUID();
            properties.clear();

            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Sleep;
            event.created_at = "1396013332"; //  Fri, 28 Mar 2014 13:28:52 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.SleepDuration;
            prop1.value = "3"; // 60 min
            properties.add(prop1);

            prop2 = new SyncEventPropertyResponse();
            prop2.type = new SyncEventPropertyTypeResponse();
            prop2.type.name = ActionKey.SleepQuality;
            prop2.value = "1"; // Healthy Restful Sleep without Interruptions
            properties.add(prop2);

            prop3 = new SyncEventPropertyResponse();
            prop3.type = new SyncEventPropertyTypeResponse();
            prop3.type.name = ActionKey.Message;
            prop3.value = UIHelper.getStringResourceByName(this, "demoKidEventSleepMessage");
            properties.add(prop3);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 6 Photos
            eventId = java.util.UUID.randomUUID();
            properties.clear();

            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Photos;
            event.created_at = "1396014731"; //   Fri, 28 Mar 2014 13:52:11 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.Message;
            prop1.value = UIHelper.getStringResourceByName(this, "demoKidEventPhotosMessage");
            properties.add(prop1);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            dbHelper.addEventDetail(eventId.toString(), java.util.UUID.randomUUID().toString()+"_asset","assets://demo_kid_photo1.jpg");
            dbHelper.addEventDetail(eventId.toString(), java.util.UUID.randomUUID().toString()+"_asset","assets://demo_kid_photo2.jpg");
            dbHelper.addEventDetail(eventId.toString(), java.util.UUID.randomUUID().toString()+"_asset","assets://demo_kid_photo3.jpg");
            dbHelper.addEventDetail(eventId.toString(), java.util.UUID.randomUUID().toString()+"_asset","assets://demo_kid_photo4.jpg");

            // 7 SignOut
            eventId = java.util.UUID.randomUUID();
            properties.clear();
            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Signout;
            event.created_at = "1396018856"; // Fri, 28 Mar 2014 15:00:56 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.Datetime;
            prop1.value = "1396018856";
            properties.add(prop1);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);

            // 8 Absence
            eventId = java.util.UUID.randomUUID();
            properties.clear();
            event = new SyncEventResponse();
            event.id = eventId.toString();
            event.action = ActionType.Absence;
            event.created_at = "1396254310"; // Mon, 31 Mar 2014 08:25:10 GMT
            event.teacher = teacher;

            prop1 = new SyncEventPropertyResponse();
            prop1.type = new SyncEventPropertyTypeResponse();
            prop1.type.name = ActionKey.AbsenceReason;
            prop1.value = "1"; //sickness
            properties.add(prop1);

            prop2 = new SyncEventPropertyResponse();
            prop2.type = new SyncEventPropertyTypeResponse();
            prop2.type.name = ActionKey.Message;
            prop2.value = UIHelper.getStringResourceByName(this, "demoKidEventAbsenceMessage");
            properties.add(prop2);
            event.properties = properties;
            dbHelper.addEvent(listenerId, externalId, event);
        }
        catch (Exception ex){
        }
    }*/
}