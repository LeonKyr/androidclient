package com.lmp.ParentMessenger.Activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.lmp.ParentMessenger.Adapters.ImageAdapter;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Background.EventHelper;
import com.lmp.ParentMessenger.Domain.Event;
import com.lmp.ParentMessenger.Domain.EventDetail;
import com.lmp.ParentMessenger.Domain.Kid;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Domain.ActionKey;
import com.mbalychev.Shared.Domain.ActionType;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.ArrayList;

public class EventDetailActivity extends PMActivity {

    int eventId;
    Event event;
    DatabaseHelper dbHelper;

    ImageView kidImageView;
    TextView teacherNameText;
    TextView kidNameText;
    TextView timeText;
    ViewStub eventBodyStub;

    ArrayList<String> thumbImagePaths;
    ArrayList<String> fullImagePaths;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_eventdetail);

        kidImageView = (ImageView)findViewById(R.id.eventdetail_kid_image);
        teacherNameText = (TextView)findViewById(R.id.eventdetail_teachername);
        kidNameText = (TextView)findViewById(R.id.eventdetail_kidname);
        timeText = (TextView)findViewById(R.id.eventdetail_time);
        eventBodyStub = (ViewStub)findViewById(R.id.eventdetail_body);

        Intent intent = getIntent();
        eventId = intent.getIntExtra("id",0);

        if(eventId == 0)
            finish();
        else{
            dbHelper = new DatabaseHelper(getApplicationContext());
            event = dbHelper.getEvent(eventId);
            Kid kid = dbHelper.getKid(0, event.getkidExternalId(), null);

            teacherNameText.setText(event.getTeacherName());
            kidNameText.setText(kid.getName());

            if(kid.getImage() != null)
               kidImageView.setImageBitmap(ImageHelper.getBitmap(kid.getImage().getData()));

            timeText.setText(EventHelper.DateFormatIventDetails.format(event.getCreatedOn() * 1000));

            if(event.getType().equals(ActionType.Note))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_note);
                View body = eventBodyStub.inflate();

                TextView noteTitle = (TextView)body.findViewById(R.id.eventdetail_title);
                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);

                String title = event.GetDetailValue(ActionKey.Title);
                String message = event.GetDetailValue(ActionKey.Message);

                noteTitle.setText(title);
                noteMessage.setText(message);
            }
            else if(event.getType().equals(ActionType.Signin)){
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_signin);
                View body = eventBodyStub.inflate();

                TextView signTime = (TextView)body.findViewById(R.id.eventdetail_signtime);
                signTime.setText(EventHelper.DateFormatIventDetails.format(getSignTime(event)));
            }
            else if(event.getType().equals(ActionType.Signout)){
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_signout);
                View body = eventBodyStub.inflate();

                TextView signTime = (TextView)body.findViewById(R.id.eventdetail_signtime);
                signTime.setText(EventHelper.DateFormatIventDetails.format(getSignTime(event)));
            }
            else if(event.getType().equals(ActionType.Photos))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_photos);
                View body = eventBodyStub.inflate();

                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);
                //String message = event.GetDetailValue(ActionKey.Message);

                GridView gridView = (GridView)body.findViewById(R.id.eventdetail_images_gridview);
                gridView.setOnItemClickListener(imagesGridViewListener);
                int columnWidth = InitilizeGridLayout(gridView);

                thumbImagePaths = new ArrayList<String>();
                fullImagePaths = new ArrayList<String>();

                for(EventDetail detail: event.getEventDetails()){
                    String key = detail.getKey();
                    if(key != null){
                        if(key.endsWith("_thumb")/* || key.endsWith("_asset")*/)
                            thumbImagePaths.add(detail.getValue());
                        else if(key.endsWith("_full"))
                            fullImagePaths.add(detail.getValue());
                    }
                }

                noteMessage.setText(R.string.NewImagesText);

                ImageAdapter adapter = new ImageAdapter(EventDetailActivity.this, thumbImagePaths, columnWidth);
                gridView.setAdapter(adapter);
            }
            else if(event.getType().equals(ActionType.Sleep))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_sleep);
                View body = eventBodyStub.inflate();

                TextView sleepDuration = (TextView)body.findViewById(R.id.eventdetail_duration);
                TextView sleepQuality = (TextView)body.findViewById(R.id.eventdetail_quality);
                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);

                String duration = UIHelper.getStringResourceByName(this, "sleepDuration" + event.GetDetailValue(ActionKey.SleepDuration));
                String quality = UIHelper.getStringResourceByName(this,"sleepQuality"+event.GetDetailValue(ActionKey.SleepQuality));

                String message = event.GetDetailValue(ActionKey.Message);

                sleepDuration.setText(duration);
                sleepQuality.setText(quality);
                noteMessage.setText(message);
            }
            else if(event.getType().equals(ActionType.Meal))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_meal);
                View body = eventBodyStub.inflate();

                TextView mealType = (TextView)body.findViewById(R.id.eventdetail_type);
                TextView mealQuality = (TextView)body.findViewById(R.id.eventdetail_quality);
                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);

                String mealtype = UIHelper.getStringResourceByName(this, "mealType"+event.GetDetailValue(ActionKey.MealType));
                String quality = UIHelper.getStringResourceByName(this, "mealQuality"+event.GetDetailValue(ActionKey.MealQuality));

                String message = event.GetDetailValue(ActionKey.Message);

                mealType.setText(mealtype);
                mealQuality.setText(quality);
                noteMessage.setText(message);
            }
            else if(event.getType().equals(ActionType.Absence))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_absence);
                View body = eventBodyStub.inflate();

                TextView absenceDesc = (TextView)body.findViewById(R.id.eventdetail_absentDesc);
                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);

                String template = UIHelper.getStringResourceByName(this, "kidMarkedAbsent");
                String reason = UIHelper.getStringResourceByName(this,"absenceReason"+event.GetDetailValue(ActionKey.AbsenceReason));

                String desc = String.format(template, reason);
                String message = event.GetDetailValue(ActionKey.Message);

                absenceDesc.setText(desc);
                noteMessage.setText(message);
            }
            else if(event.getType().equals(ActionType.WillHaveMeal))
            {
                eventBodyStub.setLayoutResource(R.layout.activity_eventdetail_willhavemeal);
                View body = eventBodyStub.inflate();

                TextView absenceDesc = (TextView)body.findViewById(R.id.eventdetail_willhavemealDesc);
                TextView noteMessage = (TextView)body.findViewById(R.id.eventdetail_message);

                String template = UIHelper.getStringResourceByName(this,"kidWillHaveMeal");
                String mealtype = UIHelper.getStringResourceByName(this,"mealType"+event.GetDetailValue(ActionKey.MealType));

                String desc = String.format(template, mealtype);
                String message = event.GetDetailValue(ActionKey.Message);

                absenceDesc.setText(desc);
                noteMessage.setText(message);
            }
        }
    }

    private int InitilizeGridLayout(GridView gridView) {
        Resources r = getResources();
        int gridPadding = 8;
        int colsNum = 3;

        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, gridPadding, r.getDisplayMetrics());

        int columnWidth = (int) ((ImageHelper.getScreenWidth(this) - ((colsNum + 1) * padding)) / colsNum);

        gridView.setNumColumns(3);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);

        return columnWidth;
    }

    private long getSignTime(Event event){
        String time = event.GetDetailValue(ActionKey.Datetime);
        long timeMilis = 0;
        if(time != null){
            timeMilis = Long.valueOf(time);
        }
        return timeMilis;
    }

    AdapterView.OnItemClickListener imagesGridViewListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //int position = Integer.valueOf(v.getTag().toString());

            Event event = dbHelper.getEvent(eventId);

            ArrayList<String> filePaths = new ArrayList<String>();
            for(EventDetail detail: event.getEventDetails()){
                String key = detail.getKey();
                if(key != null){
                    if(key.endsWith("_thumb")){
                        String fullFileKey = key.replace("_thumb","_full");

                        String fullFilePath = event.GetDetailValue(fullFileKey);
                        if(fullFilePath != null)
                            filePaths.add("file://" + fullFilePath);
                        else
                            filePaths.add(PMHelper.ServiceUrl + "image/"+key.replace("_thumb",""));
                    }/*else if(key.endsWith("_asset")){
                        filePaths.add(detail.getValue());
                    }*/
                }
            }

            Intent i = new Intent(EventDetailActivity.this, FullScreenImageViewActivity.class);
            i.putStringArrayListExtra("filePaths", filePaths);
            i.putExtra("position",position);
            i.putExtra("eventExternalId", event.getExternalId());
            startActivity(i);
        }
    };

    /*public void photoClick(View v){
        int position = Integer.valueOf(v.getTag().toString());

        Event event = dbHelper.getEvent(eventId);

        ArrayList<String> filePaths = new ArrayList<String>();
        for(EventDetail detail: event.getEventDetails()){
            if(detail.getKey() != null && detail.getKey().endsWith("_thumb")){
                String fullFileKey = detail.getKey().replace("_thumb","_full");

                String fullFilePath = event.GetDetailValue(fullFileKey);
                if(fullFilePath != null)
                    filePaths.add("file://" + fullFilePath);
                else
                    filePaths.add(PMHelper.ServiceUrl + "image/"+detail.getKey().replace("_thumb",""));
            }
        }

        Intent i = new Intent(EventDetailActivity.this, FullScreenImageViewActivity.class);
        i.putStringArrayListExtra("filePaths", filePaths);
        i.putExtra("position",position);
        i.putExtra("eventExternalId", event.getExternalId());
        startActivity(i);
    }*/

    public void thankYouClick(View v){
        finish();

        WebServiceHelper.TryThankYou(this, getCurrentListenerId(), event.getExternalId());
    }
}