package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lmp.ParentMessenger.Adapters.ActivitiesAdapter;
import com.lmp.ParentMessenger.Adapters.EventsAdapter;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Event;
import com.lmp.ParentMessenger.Domain.ScheduledActivity;
import com.lmp.ParentMessenger.R;

import java.util.List;

public class ActivitiesActivity extends PMActivity {
    private ListView activitieslistView;
    ActivitiesAdapter activitiesAdapter;
    DatabaseHelper dbHelper;
    private List<ScheduledActivity> activities;
    SharedPreferences prefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities);
        dbHelper = new DatabaseHelper(getApplicationContext());

        activitieslistView = (ListView) findViewById(R.id.activities_list);

        activitieslistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //ScheduledActivity activity = activities.get(position);
                //Intent intent = new Intent(ActivitiesActivity.this, ActivityDetailActivity.class);
                //intent.putExtra("id", activity.getId());
                //startActivity(intent);
            }
        });

        activities = dbHelper.readScheduledActivities(getCurrentListenerId(),0,null);
        activitiesAdapter = new ActivitiesAdapter(getApplicationContext(), R.layout.activities_list_row, activities);
        activitieslistView.setAdapter(activitiesAdapter);

        ActionBar bar = getActionBar();
        bar.setSubtitle(getString(R.string.activities));
    }
}