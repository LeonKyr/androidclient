package com.lmp.ParentMessenger.Activities;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Domain.ActionKey;
import com.mbalychev.Shared.Helpers.UIHelper;


public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 235478;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Demo";

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        //GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        //String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            /*if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                for (int i = 0; i < 5; i++) {
                    Log.i(TAG, "Working... " + (i + 1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification("Received: " + extras.toString());
                Log.i(TAG, "Received: " + extras.toString());
            }*/
            Log.i(TAG, "Received: " + extras.toString());
            sendNotification(extras.getString("action"));
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String action) {
        mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(getApplicationContext(), EventsActivity.class);
        intent.putExtra("isPush",true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);

        String template = UIHelper.getStringResourceByName(this, "newMessageIsReceived");

        String reason = null;
        try{
            action = action.replace("-","");
            reason = UIHelper.getStringResourceByName(this,"action_"+action+"_type");
        }catch(Exception ex){
            reason = UIHelper.getStringResourceByName(this,"action_note_type");
        }
        String message = String.format(template, reason);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.pm_icon)
                        .setContentTitle("Parent Messenger")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setContentIntent(contentIntent)
                        ;

        //long when = System.currentTimeMillis();
        Notification notification = mBuilder.build();

                //new Notification(R.drawable.pm_icon, message, when);
        //notification.flags |= Notification.FLAG_AUTO_CANCEL;

        mNotificationManager.notify(NOTIFICATION_ID, notification);

        //WebServiceHelper.sendSyncFinishedBroadcast(this);
        Intent broadcast = new Intent();
        broadcast.setAction(PMHelper.SyncPushBroadcastAction);
        sendBroadcast(broadcast);
    }
}