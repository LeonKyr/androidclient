package com.lmp.ParentMessenger.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lmp.ParentMessenger.Adapters.EventsAdapter;
import com.lmp.ParentMessenger.Domain.Event;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.PushNotificationHelper;
import com.lmp.ParentMessenger.R;

import java.util.List;

// http://kmansoft.com/2010/11/16/adding-group-headers-to-listview/
// http://code.google.com/p/android-amazing-listview/source/browse/trunk/AmazingListViewDemo/src/com/foound/amazinglistview/demo/PaginationDemoActivity.java

//https://github.com/dmitry-zaitsev/AndroidSideMenu
// https://github.com/survivingwithandroid/Surviving-with-android/blob/master/EndlessAdapter/src/com/survivingwithandroid/endlessadapter/MainActivity.java
// https://github.com/weixiao1984/Android-Infinite-Scroll-Listview

public class EventsActivity extends SliderBaseActivity{
    private ListView eventslistView;
    EventsAdapter eventsAdapter;

    private MenuItem refreshMenuItem;
    private List<Event> events;
    //private LinearLayout eventsListFooter;

    SharedPreferences prefs;
    private boolean isLoading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.events_list, R.layout.left_slider_menu_full, R.string.events);

        eventslistView = (ListView) findViewById(R.id.events_list);

        //eventsListFooter = (LinearLayout)getLayoutInflater().inflate(R.layout.events_list_footer, null);

        eventslistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Event event = events.get(position);
                Intent intent = new Intent(EventsActivity.this, EventDetailActivity.class);
                intent.putExtra("id",event.getId());
                startActivity(intent);
            }
        });

        eventslistView.setOnScrollListener(endlessScrollListener);
        prefs = getSharedPreferences(PMHelper.sharedPrefsName, MODE_PRIVATE);

        //swapAdapter();
        events = getEvents(0);
        eventsAdapter = new EventsAdapter(getApplicationContext(), R.layout.events_list_row, events, getKidImages());
        eventslistView.setAdapter(eventsAdapter);

        PushNotificationHelper.registerForPushNotifications(this);
    }

    @Override
    public void KidSelectionChanged(){
        eventsAdapter.clear();
        eventsAdapter.kidImages = getKidImages();
        eventsAdapter.addAll(getEvents(0));
        eventsAdapter.notifyDataSetChanged();

        if (refreshMenuItem != null) {
            refreshMenuItem.collapseActionView();
            refreshMenuItem.setActionView(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refreshMenuItem = item;

                refreshMenuItem.setActionView(R.layout.actionbar_progressbar);
                refreshMenuItem.expandActionView();

                ((PMApplication)getApplicationContext()).runSyncTask(getCurrentListenerId());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int tutorialShownCount = prefs.getInt("tutorialAfterLoginShowCount", 0);
        if(!PMHelper.istutorialAfterLoginAlreadyShown  && tutorialShownCount < PMHelper.tutorialAfterLoginShowCount){
            PMHelper.istutorialAfterLoginAlreadyShown = true;
            prefs.edit().putInt("tutorialAfterLoginShowCount", ++tutorialShownCount).commit();

            ShowTutorial();
        }
    }

    private void ShowTutorial(){
        Intent intent = new Intent(EventsActivity.this, TutorialActivity.class);
        intent.putExtra(PMHelper.tutorialImages, PMHelper.tutorialAfterLoginImagesArray);
        intent.putExtra(PMHelper.redirectToExtra, "close");
        startActivity(intent);
    }

    /*protected void swapAdapter1() {
        events = dbHelper.readEvents(getCurrentListenerId(),true);
        eventsAdapter = new EventsAdapter(getApplicationContext(), R.layout.events_list_row, events, getKidImages());
        eventslistView.setAdapter(eventsAdapter);
    }*/

    protected List<Event> getEvents(int lastEventId) {
        List<Event> newEvents = dbHelper.readEvents(getCurrentListenerId(),true, lastEventId, 10);
        isLoading = false;
        return newEvents;
    }

    AbsListView.OnScrollListener endlessScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (eventsAdapter == null)
                return ;

            if (eventsAdapter.getCount() == 0)
                return ;

            int l = visibleItemCount + firstVisibleItem;
            if (l >= totalItemCount && !isLoading) {
                if(totalItemCount > 10){
                    //eventslistView.addFooterView(eventsListFooter);
                    isLoading = true;

                    Event lastEvent = events.get(totalItemCount - 1);
                    List<Event> newEvents = getEvents(lastEvent.getId());
                    if(newEvents.size() > 0){
                        events.addAll(newEvents);
                        eventsAdapter.notifyDataSetChanged();
                    }
                    //eventslistView.removeFooterView(eventsListFooter);
                }
            }
        }
    };
}

