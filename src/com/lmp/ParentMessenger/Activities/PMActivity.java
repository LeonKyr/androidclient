package com.lmp.ParentMessenger.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.lmp.ParentMessenger.Domain.Listener;

public abstract class PMActivity extends Activity {

    public Listener getCurrentListener() {
        return ((PMApplication)getApplicationContext()).getCurrentListener();
    }

    public void setCurrentListener(Listener listener){
        ((PMApplication)getApplicationContext()).setCurrentListener(listener);
    }

    public String getCurrentListenerId(){
        if(getCurrentListener() != null)
            return getCurrentListener().getExternalListenerId();

        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(getCurrentListener() == null /*|| dbHelper == null*/)
        {
            redirectToLogin();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void redirectToLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}