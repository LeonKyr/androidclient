package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.R;
import com.lmp.ParentMessenger.Synchronization.SyncFeedbackRequest;
import com.lmp.ParentMessenger.Synchronization.SyncListenerResponse;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Required;

public class FeedbackActivity extends PMActivity implements Validator.ValidationListener{
    EditText subjectText;

    @Required(order = 1)
    EditText messageText;

    DatabaseHelper dbHelper;
    Validator validator;

    ProgressDialog dialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);

        dbHelper = new DatabaseHelper(getApplicationContext());

        subjectText = (EditText)findViewById(R.id.feedback_subject);
        messageText = (EditText)findViewById(R.id.feedback_message);

        validator = new Validator(this);
        validator.setValidationListener(this);

        dialog  = new ProgressDialog(this);

        ActionBar bar = getActionBar();
        bar.setTitle(getString(R.string.feedback));
    }

    @Override
    public void onValidationSucceeded() {
        final String subject = subjectText.getText().toString();
        final String message = messageText.getText().toString();

        if(UIHelper.hasNetwork(getApplicationContext()))
        {
            dialog.setMessage(getString(R.string.pleaseWait));
            dialog.show();

            SyncFeedbackRequest request = new SyncFeedbackRequest();
            request.source = subject;
            request.text = message;

            Http http = HttpFactory.create(getApplicationContext());
            http.post(PMHelper.ServiceUrl+"feedback/listener/"+getCurrentListenerId()+"/")
                    .data(request)
                    .handler(new ResponseHandler<SyncListenerResponse>(){
                        @Override
                        public void success(SyncListenerResponse response, HttpResponse httpResponse)
                        {
                            finish();
                        }

                        @Override
                        public void error(String message, HttpResponse response) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void failure(NetworkError error) {
                            UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                        }

                        @Override
                        public void complete() {
                            dialog.dismiss();

                        }
                    }).send();

            //redirectToMainScreen();
        }else{
            UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        PMHelper.validationFailedRule(getApplicationContext(), failedView, failedRule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_saveonly, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                validator.validate();
                break;
            default:
                break;
        }

        return true;
    }
}