package com.lmp.ParentMessenger.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.kodart.httpzoid.*;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.Helpers.WebServiceHelper;
import com.lmp.ParentMessenger.R;
import com.lmp.ParentMessenger.Synchronization.SyncListenerResponse;
import com.lmp.ParentMessenger.Synchronization.SyncObjectWithDeviceRequest;
import com.mbalychev.Shared.Helpers.AeSimpleSHA1;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;

public class LoginActivity extends Activity implements Validator.ValidationListener {
    @Required(order = 1)
    @Email(order = 2, message = "Enter valid email")
    EditText emailEditText;

    @Required(order = 3)
    EditText passwordEditText;

    CheckBox isAutoLoginCheckBox;
    DatabaseHelper dbHelper;

    ProgressDialog dialog;

    Validator validator;
    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);

        Button btnLogin = (Button)findViewById(R.id.btnLogin);

        emailEditText = (EditText)findViewById(R.id.login_email);
        passwordEditText = (EditText)findViewById(R.id.login_password);
        isAutoLoginCheckBox = (CheckBox)findViewById(R.id.is_autologin);

        TextView forgotPassword = (TextView) findViewById(R.id.forgot_password);

        dbHelper = new DatabaseHelper(getApplicationContext());
        prefs = getSharedPreferences(PMHelper.sharedPrefsName, MODE_PRIVATE);

        // Listening to activity_register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0)
            {
                validator.validate();
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);

        dialog  = new ProgressDialog(LoginActivity.this);

        // TODO: remove after testing
        //emailEditText.setText("test2@test.com");
        //passwordEditText.setText("111111");
    }

    private void redirectToMainScreen() {
        Intent intent = new Intent(this, EventsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        final String email = emailEditText.getText().toString();
        String password =  passwordEditText.getText().toString();

        try{
            final String hashPassword = AeSimpleSHA1.MD5(password);

            if(UIHelper.hasNetwork(getApplicationContext()))
            {
                dialog.setMessage(getString(R.string.pleaseWait));
                dialog.show();

                SyncObjectWithDeviceRequest request = new SyncObjectWithDeviceRequest();
                String deviceId = dbHelper.getOrCreateSetting(PMHelper.deviceIdPrefKey, null);
                request.device = WebServiceHelper.GetDevice(deviceId);

                Http http = HttpFactory.create(getApplicationContext());
                http.post(PMHelper.ServiceUrl+"login/listener/"+email+"/"+hashPassword+"/")
                        .data(request)
                        .handler(new ResponseHandler<SyncListenerResponse>(){
                            @Override
                            public void success(SyncListenerResponse response, HttpResponse httpResponse)
                            {
                                Listener listener = dbHelper.getListener(response.id);
                                if(listener == null){
                                    dbHelper.addListener(response.id, response.name, response.email, hashPassword, isAutoLoginCheckBox.isChecked(), null);
                                }else{
                                    dbHelper.updateListener(response.id, response.name, response.email, isAutoLoginCheckBox.isChecked(), 0);
                                }

                                listener = dbHelper.getListener(email,hashPassword);
                                ((PMApplication)getApplicationContext()).setCurrentListener(listener);

                                redirectToMainScreen();

                                ((PMApplication)getApplicationContext()).runSyncTask(response.id);
                            }

                            @Override
                            public void error(String message, HttpResponse response) {
                                UIHelper.showToast(getApplicationContext(), getString(R.string.serviceError), Toast.LENGTH_LONG, UIHelper.TDError);
                            }

                            @Override
                            public void failure(NetworkError error) {
                                UIHelper.showToast(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG, UIHelper.TDError);
                            }

                            @Override
                            public void complete() {
                                dialog.dismiss();

                                // TODO: REMOVE!!!
                                //Listener listener = dbHelper.getListener(email,hashPassword);
                                //((PMApplication)getApplicationContext()).setCurrentListener(listener);

                            }
                        }).send();

                //redirectToMainScreen();
            }else{
                UIHelper.showToast(getApplicationContext(), getString(R.string.noConnection), Toast.LENGTH_LONG, UIHelper.TDError);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            UIHelper.showToast(getApplicationContext(), message, Toast.LENGTH_SHORT, UIHelper.TDError);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Listener activeListener = dbHelper.getActiveListener();
        if(activeListener != null){
            if(activeListener.getIsAutoLogin()){
                ((PMApplication)getApplicationContext()).setCurrentListener(activeListener);
                startActivity(new Intent(LoginActivity.this, EventsActivity.class));

                ((PMApplication)getApplicationContext()).runSyncTask(activeListener.getExternalListenerId());
            }
        }
    }
}