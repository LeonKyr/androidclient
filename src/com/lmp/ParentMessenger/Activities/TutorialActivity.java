package com.lmp.ParentMessenger.Activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.lmp.ParentMessenger.Adapters.TutorialAdapter;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.ZoomOutPageTransformer;
import com.viewpagerindicator.CirclePageIndicator;

public class TutorialActivity extends FragmentActivity {
    ViewPager viewPager;
    CirclePageIndicator indicator;
    TutorialAdapter adapter;
    String redirectTo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_tutorial);

        Intent inputIntent = getIntent();
        redirectTo = inputIntent.getStringExtra(PMHelper.redirectToExtra);

        int[] imageIds = inputIntent.getIntArrayExtra(PMHelper.tutorialImages);

        viewPager = (ViewPager)findViewById(R.id.tutorialPager);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        indicator = (CirclePageIndicator)findViewById(R.id.tutorialIndicator);

        adapter = new TutorialAdapter(getSupportFragmentManager(), imageIds);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);

        // set window attributes
        Rect displayRectangle = new Rect();
        Window window = this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.height = (int)(displayRectangle.height() * 0.98f);
        params.width = (int)(displayRectangle.width() * 0.98f);
        params.alpha = 1.0f;
        params.dimAmount = 0.5f;
        this.getWindow().setAttributes(params);
    }

    public void onClose(View v){
        Intent intent = null;
        if(redirectTo.equals("register"))
            intent = new Intent(TutorialActivity.this, RegisterActivity.class);
        //else if(redirectTo.equals("main"))
        //    intent = new Intent(TutorialActivity.this, MainActivity.class);

        if(intent != null)
            startActivity(intent);

        finish();
    }
}