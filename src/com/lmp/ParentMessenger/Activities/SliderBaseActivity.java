package com.lmp.ParentMessenger.Activities;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Domain.IKidSelectionChanged;
import com.lmp.ParentMessenger.Domain.Kid;
import com.lmp.ParentMessenger.Domain.Listener;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SliderBaseActivity extends PMActivity implements IKidSelectionChanged {
    //private SlideHolder sliderHolder;
    //private ViewStub mainView;
    //private ViewStub sliderView;

    SlidingMenu menu;

    DatabaseHelper dbHelper;

    ImageView listenerImage;
    List<Kid> chilren;
    TableLayout chilrenList;
    TextView chilrenListEmpty;

    private Map<String, Bitmap> kidImages = new HashMap<String, Bitmap>();

    public Map<String, Bitmap> getKidImages(){
        return kidImages;
    }


    public void onCreate(Bundle savedInstanceState, int mainLayoutId, int menuLayoutId, int titleId) {
        super.onCreate(savedInstanceState);

        /*setContentView(R.layout.slider_base);

        sliderHolder = (SlideHolder) findViewById(R.id.slideHolder);

        mainView = (ViewStub)findViewById(R.id.mainview_placeholder);
        mainView.setLayoutResource(mainLayoutId);
        mainView.inflate();

        sliderView = (ViewStub)findViewById(R.id.slider_placeholder);
        sliderView.setLayoutResource(menuLayoutId);
        sliderView.inflate();
        */
        setContentView(mainLayoutId);

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setBehindWidth(UIHelper.dpToPx(getApplicationContext(), 200));
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(menuLayoutId);

        listenerImage = (ImageView)findViewById(R.id.listener_image);
        chilrenList = (TableLayout)findViewById(R.id.children_list);
        chilrenListEmpty = (TextView)findViewById(R.id.children_empty_text);


        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setSubtitle(getString(titleId));

        dbHelper = new DatabaseHelper(getApplicationContext());
        updateAdapter();

        IntentFilter filter = new IntentFilter();
        filter.addAction(PMHelper.SyncFinishedBroadcastAction);
        registerReceiver(receiver, filter);

        IntentFilter filter2 = new IntentFilter();
        filter2.addAction(PMHelper.SyncPushBroadcastAction);
        registerReceiver(receiverPush, filter2);
    }

    public void toogleMenu(){
        menu.toggle(true);
        //sliderHolder.toggle();
        //sliderHolder.destroyDrawingCache();
    }

    public void onSliderItemClick(View v){
        int viewId = v.getId();

        Intent intent = null;
        switch (viewId)
        {
            case R.id.slider_logout:
                dbHelper.deactivateActiveListener();
                intent = new Intent(this, LoginActivity.class);
                break;
            case R.id.slider_addchild:
                intent = new Intent(this, AddChildActivity.class);
                break;
            //case R.id.slider_settings:
            //    intent = new Intent(this, SettingsActivity.class);
            //    break;
            case R.id.slider_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                break;
            case R.id.slider_profile:
                intent = new Intent(this, ListenerEditActivity.class);
                break;
            case R.id.slider_tutorial:
                intent = new Intent(this, TutorialActivity.class);
                intent.putExtra(PMHelper.tutorialImages, PMHelper.tutorialAfterLoginImagesArray);
                intent.putExtra(PMHelper.redirectToExtra, "close");
                break;
            //case R.id.slider_activities:
            //    intent = new Intent(this, ActivitiesActivity.class);
            //    break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            toogleMenu();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Listener listener = getCurrentListener();

        if(listener == null)
        {
           redirectToLogin();
        }
        else{
            if(getCurrentListener().getImage() != null)
            {
                listenerImage.setImageBitmap(ImageHelper.getBitmap(getCurrentListener().getImage().getData()));
            }else{
                listenerImage.setImageResource(R.drawable.pm_default_photo);
            }
        }
    }

    @Override
    protected void onPause() {
        //unregisterReceiver(receiver);
        super.onPause();
    }

    private void updateAdapter(){
        chilren  = dbHelper.readKids(getCurrentListenerId(), false);

        if(chilren.size() == 0) {
            chilrenListEmpty.setVisibility(View.VISIBLE);
            chilrenList.setVisibility(View.GONE);
        }else{
            chilrenListEmpty.setVisibility(View.GONE);
            chilrenList.setVisibility(View.VISIBLE);
            //chilrenList.setAdapter(new ChildrenAdapter(getApplicationContext(), R.layout.kid_row, chilren));

            chilrenList.removeAllViews();

            TextView tv;
            kidImages = new HashMap<String, Bitmap>();

            for(Kid kid:chilren){
                TableRow row = getNewTableRow(kid);
                chilrenList.addView(row);

                row.setTag(kid.getId());
                row.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox isSelected = (CheckBox)v.findViewById(R.id.kid_row_isselected);
                        Long kidId = Long.valueOf(v.getTag().toString());

                        boolean isChecked = !isSelected.isChecked();
                        isSelected.setChecked(isChecked);

                        dbHelper.updateKidIsSelected(kidId, isChecked);

                        // fire notification
                        KidSelectionChanged();
                    }
                });

                // Draw separator
                tv = new TextView(this);
                tv.setBackgroundColor(Color.parseColor("#80808080"));
                tv.setHeight(2);
                chilrenList.addView(tv);
            }
        }
    }

    public TableRow getNewTableRow(Kid data) {

        LayoutInflater inflater = LayoutInflater.from(this.getApplicationContext());
        TableRow row = (TableRow) inflater.inflate(R.layout.kid_row, null);

        TextView name = (TextView)row.findViewById(R.id.kid_row_name);
        ImageView image = (ImageView)row.findViewById(R.id.kid_row_image);
        CheckBox isSelected = (CheckBox)row.findViewById(R.id.kid_row_isselected);

        name.setText(data.getName());

        if(data.getImage() != null){
            Bitmap bm = ImageHelper.getBitmap(data.getImage().getData());
            kidImages.put(data.getExternalId(), bm);
            image.setImageBitmap(bm);
        }else
            image.setImageResource(R.drawable.pm_default_photo);

        isSelected.setChecked(data.getIsSelected());

        return row;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Integer totalUpdates = intent.getIntExtra("total",0);
            Integer successUpdates = intent.getIntExtra("success",0);

            if(totalUpdates > 0 && successUpdates > 0)
            {
                updateAdapter();
                KidSelectionChanged();
            }
        }
    };

    private BroadcastReceiver receiverPush = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ((PMApplication)getApplicationContext()).runSyncTask(getCurrentListenerId());
       }
    };
}