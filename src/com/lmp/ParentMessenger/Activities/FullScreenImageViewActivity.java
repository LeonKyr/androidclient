package com.lmp.ParentMessenger.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.lmp.ParentMessenger.Background.DatabaseHelper;
import com.lmp.ParentMessenger.Helpers.PMHelper;
import com.lmp.ParentMessenger.R;
import com.mbalychev.Shared.Activities.ImagePickerActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class FullScreenImageViewActivity extends Activity
{
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private ViewPager viewPager;

    private static final String STATE_POSITION = "position";
    private String eventExternalId;
    private DisplayImageOptions options;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_fullscreen);

        Intent i = getIntent();
        int position = i.getIntExtra(STATE_POSITION, 0);
        eventExternalId = i.getStringExtra("eventExternalId");
        ArrayList<String> thumbFilePaths = i.getStringArrayListExtra("filePaths");

        if(eventExternalId == null || thumbFilePaths.size() == 0){
            finish();
        }else{
            dbHelper = new DatabaseHelper(getApplicationContext());

            if (savedInstanceState != null) {
                position = savedInstanceState.getInt(STATE_POSITION);
            }

            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.pm_default_photo)
                    .showImageOnFail(R.drawable.pm_default_photo)
                    .resetViewBeforeLoading(true)
                    .cacheOnDisc(false)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new FadeInBitmapDisplayer(300))
                    .build();

            viewPager = (ViewPager) findViewById(R.id.image_pager);

            viewPager.setAdapter(new ImagePagerAdapter(thumbFilePaths.toArray(new String[thumbFilePaths.size()])));
            viewPager.setCurrentItem(position);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_POSITION, viewPager.getCurrentItem());
    }

    private class ImagePagerAdapter extends PagerAdapter {

        private String[] images;
        private LayoutInflater inflater;

        ImagePagerAdapter(String[] images) {
            this.images = images;
            inflater = getLayoutInflater();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object instantiateItem(ViewGroup view, final int position) {
            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            assert imageLayout != null;
            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);

            imageLoader.displayImage(images[position], imageView, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    spinner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    String message = null;
                    switch (failReason.getType()) {
                        case IO_ERROR:
                            message = "Input/Output error";
                            break;
                        case DECODING_ERROR:
                            message = "Image can't be decoded";
                            break;
                        case NETWORK_DENIED:
                            message = "Downloads are denied";
                            break;
                        case OUT_OF_MEMORY:
                            message = "Out Of Memory error";
                            break;
                        case UNKNOWN:
                            message = "Unknown error";
                            break;
                    }
                    Toast.makeText(FullScreenImageViewActivity.this , message, Toast.LENGTH_SHORT).show();

                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    spinner.setVisibility(View.GONE);

                    if(imageUri.contains(PMHelper.ServiceUrl)){
                        String[] parts = imageUri.split("/");
                        String imageExternalId = parts[parts.length - 1];

                        try {
                            final File file = ImagePickerActivity.getPhotoFile(getApplicationContext());
                            if (file != null) {

                                FileOutputStream out = new FileOutputStream(file);
                                loadedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
                                out.close();

                                String filePath = file.getAbsolutePath();
                                dbHelper.addEventDetail(eventExternalId, imageExternalId+ "_full", filePath);

                                images[position] = filePath;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }
            });

            view.addView(imageLayout, 0);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }
}