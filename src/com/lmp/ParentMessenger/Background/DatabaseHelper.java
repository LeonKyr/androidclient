package com.lmp.ParentMessenger.Background;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.lmp.ParentMessenger.Domain.*;
import com.lmp.ParentMessenger.Synchronization.SyncEventPropertyResponse;
import com.lmp.ParentMessenger.Synchronization.SyncEventResponse;
import com.mbalychev.Shared.Domain.ActionType;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ParentMessengerParent.db";
    private static final int DATABASE_VERSION = 1;

    private static final String LISTENER_EXTERNALID_COLUMN = "listener_externalid";
    private static final String LISTENER_EXTERNALID_SQL = LISTENER_EXTERNALID_COLUMN + " TEXT NOT NULL,";

    private static final String EXTERNALID_COLUMN = "externalid";
    private static final String EXTERNALID_SQL = EXTERNALID_COLUMN + " TEXT,";

    private static final String MEDIA_ID_COLUMN = "mediaid";
    private static final String MEDIA_ID_SQL = MEDIA_ID_COLUMN + " INTEGER,";


    public static final String LISTENER_TABLE_NAME = "listener";
    private static final String LISTENER_TABLE_ID_COLUMN = "_id";
    private static final String LISTENER_TABLE_NAME_COLUMN = "name";
    private static final String LISTENER_TABLE_EMAIL_COLUMN = "email";
    private static final String LISTENER_TABLE_PASSWORD_HASH_COLUMN = "password_hash";
    private static final String LISTENER_TABLE_IS_AUTOLOGIN_COLUMN = "is_autologin";
    private static final String LISTENER_TABLE_IS_LAST_COLUMN = "is_last";

    private static final String LISTENER_TABLE_CREATE = "CREATE TABLE "
            + LISTENER_TABLE_NAME + "("
            + LISTENER_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EXTERNALID_SQL
            + MEDIA_ID_SQL
            + LISTENER_TABLE_NAME_COLUMN + " TEXT,"
            + LISTENER_TABLE_EMAIL_COLUMN + " TEXT,"
            + LISTENER_TABLE_IS_AUTOLOGIN_COLUMN + " int,"
            + LISTENER_TABLE_IS_LAST_COLUMN + " int,"
            + LISTENER_TABLE_PASSWORD_HASH_COLUMN + " TEXT);";

    public static final String MEDIA_TABLE_NAME = "media";
    private static final String MEDIA_TABLE_ID_COLUMN = "_id";
    private static final String MEDIA_TABLE_TYPE_COLUMN = "type";
    private static final String MEDIA_TABLE_NAME_COLUMN = "name";
    private static final String MEDIA_TABLE_DATA_COLUMN = "data";

    private static final String MEDIA_TABLE_CREATE = "CREATE TABLE "
            + MEDIA_TABLE_NAME + "("
            + MEDIA_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EXTERNALID_SQL
            + MEDIA_TABLE_TYPE_COLUMN + " TEXT,"
            + MEDIA_TABLE_NAME_COLUMN + " TEXT,"
            + MEDIA_TABLE_DATA_COLUMN + " TEXT);";

    public static final String KIDS_TABLE_NAME = "kids";
    private static final String KIDS_TABLE_ID_COLUMN = "_id";
    private static final String KIDS_TABLE_UNIQUEID_COLUMN = "unique_id";
    private static final String KIDS_TABLE_NAME_COLUMN = "name";
    private static final String KIDS_TABLE_BIRTHDAY_COLUMN = "birthday";
    private static final String KIDS_TABLE_IS_SELECTED_COLUMN = "is_selected";

    private static final String KIDS_TABLE_CREATE = "CREATE TABLE "
            + KIDS_TABLE_NAME + "("
            + KIDS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + LISTENER_EXTERNALID_SQL
            + EXTERNALID_SQL
            + KIDS_TABLE_UNIQUEID_COLUMN + " TEXT,"
            + KIDS_TABLE_NAME_COLUMN + " TEXT NOT NULL,"
            + MEDIA_ID_SQL
            + KIDS_TABLE_IS_SELECTED_COLUMN + " INT,"
            + KIDS_TABLE_BIRTHDAY_COLUMN + " TEXT);";

    public static final String EVENTS_TABLE_NAME = "events";
    private static final String EVENTS_TABLE_ID_COLUMN = "_id";
    private static final String EVENTS_TABLE_TYPE_COLUMN = "eventtype";
    private static final String EVENTS_TABLE_CREATEDON_COLUMN = "createdon";
    private static final String EVENTS_TABLE_TEACHER_EXTERNALID_COLUMN = "teacher_externalid";
    private static final String EVENTS_TABLE_TEACHER_NAME_COLUMN = "teacher_name";
    private static final String EVENTS_TABLE_KID_EXTERNALID_COLUMN = "kid_externalid";
    private static final String EVENTS_TABLE_IS_NEW_COLUMN = "is_new";

    private static final String EVENTS_TABLE_CREATE = "CREATE TABLE "
            + EVENTS_TABLE_NAME + "("
            + EVENTS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EVENTS_TABLE_TYPE_COLUMN + " TEXT NOT NULL,"
            + EVENTS_TABLE_KID_EXTERNALID_COLUMN + " TEXT NOT NULL,"
            + LISTENER_EXTERNALID_SQL
            + EXTERNALID_SQL
            + EVENTS_TABLE_TEACHER_EXTERNALID_COLUMN + " TEXT,"
            + EVENTS_TABLE_TEACHER_NAME_COLUMN + " TEXT,"
            +EVENTS_TABLE_IS_NEW_COLUMN + " INTEGER,"
            + EVENTS_TABLE_CREATEDON_COLUMN + " TEXT NOT NULL);";

    private static final String EVENT_DETAILS_TABLE_NAME = "event_details";
    private static final String EVENT_DETAILS_TABLE_ID_COLUMN = "_id";
    private static final String EVENT_DETAILS_TABLE_EVENT_EXTERNALID_COLUMN = "event_externalid";
    private static final String EVENT_DETAILS_TABLE_KEY_COLUMN = "key";
    private static final String EVENT_DETAILS_TABLE_VALUE_COLUMN = "value";

    private static final String EVENT_DETAILS_TABLE_CREATE = "CREATE TABLE "
            + EVENT_DETAILS_TABLE_NAME + "("
            + EVENT_DETAILS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EVENT_DETAILS_TABLE_EVENT_EXTERNALID_COLUMN + " INTEGER,"
            + EVENT_DETAILS_TABLE_KEY_COLUMN + " TEXT,"
            + EVENT_DETAILS_TABLE_VALUE_COLUMN + " TEXT);";

    public static final String SETTINGS_TABLE_NAME = "settings";
    private static final String SETTINGS_TABLE_ID_COLUMN = "_id";
    private static final String SETTINGS_TABLE_NAME_COLUMN = "name";
    private static final String SETTINGS_TABLE_VALUE_COLUMN = "value";

    private static final String SETTINGS_TABLE_CREATE = "CREATE TABLE "
            + SETTINGS_TABLE_NAME + "("
            + SETTINGS_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SETTINGS_TABLE_NAME_COLUMN + " TEXT,"
            + SETTINGS_TABLE_VALUE_COLUMN + " TEXT);";

    public static final String SCHEDULED_ACTIVITIES_TABLE_NAME = "scheduled_activities";
    private static final String SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN = "_id";
    private static final String SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN = "isactive";
    private static final String SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN = "type";
    private static final String SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN = "title";
    private static final String SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN = "description";
    private static final String SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN = "createdon";

    private static final String SCHEDULED_ACTIVITIES_TABLE_CREATE = "CREATE TABLE "
            + SCHEDULED_ACTIVITIES_TABLE_NAME + "("
            + SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + LISTENER_EXTERNALID_SQL
            + EXTERNALID_SQL
            + SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN + " INTEGER,"
            + SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN + " TEXT NOT NULL,"
            + SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN + " TEXT NOT NULL);";

    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_NAME = "scheduledactivity_dates";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN = "_id";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN = "scheduledactivity_id";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN = "date";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN = "reminder_type";
    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN = "reminder_message";

    private static final String SCHEDULED_ACTIVITY_DATES_TABLE_CREATE = "CREATE TABLE "
            + SCHEDULED_ACTIVITY_DATES_TABLE_NAME + "("
            + SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN   + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " INTEGER,"
            + EXTERNALID_SQL
            + SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN + " TEXT,"
            + SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(LISTENER_TABLE_CREATE);
            db.execSQL(KIDS_TABLE_CREATE);
            db.execSQL(MEDIA_TABLE_CREATE);
            db.execSQL(EVENTS_TABLE_CREATE);
            db.execSQL(EVENT_DETAILS_TABLE_CREATE);
            db.execSQL(SETTINGS_TABLE_CREATE);
            db.execSQL(SCHEDULED_ACTIVITIES_TABLE_CREATE);
            db.execSQL(SCHEDULED_ACTIVITY_DATES_TABLE_CREATE);

            Log.e("dbAdapter","DB sucessfully created");
        } catch (Exception e) {
            Log.e("dbAdapter", e.getMessage().toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public long addListener(String externalId, String name, String email, String passwordHash, boolean autoLogin, byte[] image){
        long id = 0;
        try
        {
            deactivateActiveListener();

            ContentValues values = new ContentValues();

            long mediaId = 0;
            if(image != null && image.length > 0){
                mediaId = addMedia(null, MediaType.image.toString(), "",image);
            }

            values.put(MEDIA_ID_COLUMN, mediaId);
            values.put(LISTENER_TABLE_NAME_COLUMN, name);
            values.put(LISTENER_TABLE_EMAIL_COLUMN, email);
            values.put(LISTENER_TABLE_PASSWORD_HASH_COLUMN, passwordHash);
            values.put(LISTENER_TABLE_IS_AUTOLOGIN_COLUMN, autoLogin ? 1 : 0);
            values.put(LISTENER_TABLE_IS_LAST_COLUMN, 1);

            values.put(EXTERNALID_COLUMN, externalId);

            SQLiteDatabase db = this.getWritableDatabase();

            id = db.insert(LISTENER_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    public void updateListener(String externalId, String name, String email, boolean autoLogin, long imageId){
        try
        {
            //Listener listener = getListener(externalId);

            ContentValues values = new ContentValues();

            if(imageId > 0)
                values.put(MEDIA_ID_COLUMN, imageId);
            /*Media currentImage = listener.getImage();

            if(image != null){
                if(currentImage != null)
                {
                    updateMedia(currentImage.getId(), null, "", image);
                }else{
                    long mediaId = addMedia(null, MediaType.image.toString(), "",image);
                    values.put(MEDIA_ID_COLUMN, mediaId);
                }
            }*/

            values.put(LISTENER_TABLE_NAME_COLUMN, name);
            values.put(LISTENER_TABLE_EMAIL_COLUMN, email);
            values.put(LISTENER_TABLE_IS_AUTOLOGIN_COLUMN, autoLogin ? 1 : 0);
            values.put(LISTENER_TABLE_IS_LAST_COLUMN, 1);

            String where = EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(LISTENER_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateListenerPassword(String externalId, String passwordHash){
        try
        {
            ContentValues values = new ContentValues();

            values.put(LISTENER_TABLE_PASSWORD_HASH_COLUMN, passwordHash);

            String where = EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(LISTENER_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deactivateActiveListener(){
        try
        {
            ContentValues values = new ContentValues();

            values.put(LISTENER_TABLE_IS_LAST_COLUMN, 0);
            String where = LISTENER_TABLE_IS_LAST_COLUMN + " = 1";

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(LISTENER_TABLE_NAME, values, where, null);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Listener getListener(String externalId){
        Listener listener = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + LISTENER_TABLE_NAME;
            sql += " WHERE " + EXTERNALID_COLUMN + " = ?";

            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            if (cursor.moveToFirst())
            {
                listener = GetListenerFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listener;
    }

    public Listener getListener(String email, String password){
        Listener listener = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + LISTENER_TABLE_NAME;
            sql += " WHERE " + LISTENER_TABLE_EMAIL_COLUMN + " = ?";

            String[] keys = new String[]{email};

            if(password != null){
                sql += " AND " + LISTENER_TABLE_PASSWORD_HASH_COLUMN + " = ?";
                keys = new String[]{email, password};
            }

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, keys);

            if (cursor.moveToFirst())
            {
                listener = GetListenerFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listener;
    }

    public Listener getActiveListener(){
        Listener listener = null;

        try {
            String sql = "";
            sql += "SELECT * FROM " + LISTENER_TABLE_NAME;
            sql += " WHERE " + LISTENER_TABLE_IS_LAST_COLUMN + " = 1";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                listener = GetListenerFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listener;
    }

    private Listener GetListenerFromCursor(Cursor cursor){
        int kigaId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(LISTENER_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(LISTENER_TABLE_NAME_COLUMN));
        String email = cursor.getString(cursor.getColumnIndex(LISTENER_TABLE_EMAIL_COLUMN));
        Integer isAutoLogin = cursor.getInt(cursor.getColumnIndex(LISTENER_TABLE_IS_AUTOLOGIN_COLUMN));
        String password = cursor.getString(cursor.getColumnIndex(LISTENER_TABLE_PASSWORD_HASH_COLUMN));

        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        long imageId  = cursor.getLong(cursor.getColumnIndex(MEDIA_ID_COLUMN));

        Media image = null;
        if(imageId >= 0)
            image = getMedia(imageId);

        return new Listener(externalId, kigaId, name, email, password, (isAutoLogin == 1), image);
    }

    public Media getMedia(long id) {

        Media media = null;

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + MEDIA_TABLE_NAME;
            sql += " WHERE " + MEDIA_TABLE_ID_COLUMN + " = ?";

            String[] selectionArgs = { String.valueOf(id) };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            if (cursor.moveToFirst())
            {
                media = GetMediaFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return media;
    }

    public Media getMediaByExternalId(String externalId) {

        Media media = null;

        try {
            String sql = "";
            sql += " SELECT *";
            sql += " FROM " + MEDIA_TABLE_NAME;
            sql += " WHERE " + EXTERNALID_COLUMN + " = ?";

            String[] selectionArgs = { String.valueOf(externalId) };

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, selectionArgs);

            if (cursor.moveToFirst())
            {
                media = GetMediaFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return media;
    }

    private Media GetMediaFromCursor(Cursor cursor){

        long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_NAME_COLUMN));
        String type = cursor.getString(cursor.getColumnIndex(MEDIA_TABLE_TYPE_COLUMN));
        byte[] data = cursor.getBlob(cursor.getColumnIndex(MEDIA_TABLE_DATA_COLUMN));
        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        return new Media(id, externalId, type, name, data);
    }

    public long addMedia(String externalId, String type, String name, byte[] data){
        long id = 0;
        try {
            ContentValues values = new ContentValues();

            SQLiteDatabase db = this.getWritableDatabase();

            values.put(EXTERNALID_COLUMN, externalId);
            values.put(MEDIA_TABLE_TYPE_COLUMN, type);
            values.put(MEDIA_TABLE_NAME_COLUMN, name);
            values.put(MEDIA_TABLE_DATA_COLUMN, data);

            id = db.insert(MEDIA_TABLE_NAME, null, values);

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }
    public void updateMedia(long id, String externalId, String name, byte[] data){
        try
        {
            ContentValues values = new ContentValues();

            values.put(MEDIA_TABLE_NAME_COLUMN, name);
            values.put(MEDIA_TABLE_DATA_COLUMN, data);

            if(externalId != null)
                values.put(EXTERNALID_COLUMN, externalId);

            String where = MEDIA_TABLE_ID_COLUMN + " = ?";
            String[] whereArgs = { Long.toString(id) };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(MEDIA_TABLE_NAME, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMediaId(String tableName, String externalId, long mediaId){
        try
        {
            ContentValues values = new ContentValues();

            values.put(MEDIA_ID_COLUMN, mediaId);


            String where = EXTERNALID_COLUMN + " = ?";
            String[] whereArgs = { externalId };

            SQLiteDatabase db = this.getWritableDatabase();

            db.update(tableName, values, where, whereArgs);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Kid> readKids(String listenerExternalId, Boolean onlySelected) {

        List<Kid> recordsList = new ArrayList<Kid>();

        String where = "";
        where = " WHERE k." + LISTENER_EXTERNALID_COLUMN + " = ?";

        if(onlySelected)
            where += " AND k." + KIDS_TABLE_IS_SELECTED_COLUMN + " = 1";

        String[] whereArgs = { listenerExternalId };

        try {
            String sql = "";
            sql += " SELECT k.*";
            sql += " FROM " + KIDS_TABLE_NAME + " as k";
            sql += where;
            sql += " ORDER BY k." + KIDS_TABLE_NAME_COLUMN + " DESC";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                recordsList.add(GetKidFromCursor(cursor));
                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public Kid getKid(long id, String externalId, String uniqueCode) {

        Kid kid = null;

        try {
            String where = null;

            if(id > 0)
                where = " WHERE k."+ KIDS_TABLE_ID_COLUMN + " = '"+String.valueOf(id)+"'";

            if(uniqueCode != null){
                if(where == null)
                    where = " WHERE ";
                else
                    where += " AND ";

                where += "k."+KIDS_TABLE_UNIQUEID_COLUMN + " = '"+uniqueCode+"'";
            }

            if(externalId != null){
                if(where == null)
                    where = " WHERE ";
                else
                    where += " AND ";

                where += "k."+EXTERNALID_COLUMN + " = '"+externalId+"'";
            }

            String sql = "";
            sql += " SELECT k.*";
            sql += " FROM " + KIDS_TABLE_NAME + " as k";
            sql += where;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst())
            {
                kid = GetKidFromCursor(cursor);
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kid;
    }

    private Kid GetKidFromCursor(Cursor cursor){
        int kidId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KIDS_TABLE_ID_COLUMN)));
        String name = cursor.getString(cursor.getColumnIndex(KIDS_TABLE_NAME_COLUMN));
        Long birthday = cursor.getLong(cursor.getColumnIndex(KIDS_TABLE_BIRTHDAY_COLUMN));
        String uniqueId = cursor.getString(cursor.getColumnIndex(KIDS_TABLE_UNIQUEID_COLUMN));
        String listenerExternalId = cursor.getString(cursor.getColumnIndex(LISTENER_EXTERNALID_COLUMN));
        Integer isSelected = cursor.getInt(cursor.getColumnIndex(KIDS_TABLE_IS_SELECTED_COLUMN));

        long imageId  = cursor.getLong(cursor.getColumnIndex(MEDIA_ID_COLUMN));

        Media image = null;
        if(imageId >= 0)
            image = getMedia(imageId);

        String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));

        return new Kid(listenerExternalId, kidId, externalId, name, image, birthday, uniqueId, (isSelected == 1));
    }

    public long addKid(String listenerId, String externalId, String name, long birthday, String imageExternalId, byte[] image, String uniqueId){
        long kidId = 0;
        try {
            ContentValues values = new ContentValues();

            long mediaId = 0;
            if(image != null && image.length > 0){
                mediaId = addMedia(imageExternalId, MediaType.image.toString(), "",image);
            }

            values.put(KIDS_TABLE_NAME_COLUMN, name);
            values.put(KIDS_TABLE_BIRTHDAY_COLUMN, birthday);
            values.put(MEDIA_ID_COLUMN, mediaId);
            values.put(EXTERNALID_COLUMN, externalId);
            values.put(KIDS_TABLE_UNIQUEID_COLUMN, uniqueId);
            values.put(KIDS_TABLE_IS_SELECTED_COLUMN, 1);
            values.put(LISTENER_EXTERNALID_COLUMN, listenerId);

            SQLiteDatabase db = this.getWritableDatabase();

            kidId = db.insert(KIDS_TABLE_NAME, null, values);
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return kidId;
    }

    public void updateKidIsSelected(long kidId, Boolean isSelected){
        try{
            ContentValues values = new ContentValues();

            values.put(KIDS_TABLE_IS_SELECTED_COLUMN, isSelected ? "1" : "0");

            SQLiteDatabase db = this.getWritableDatabase();

            String where = KIDS_TABLE_ID_COLUMN + " = ?";
            String[] whereArgs = new String[]{ String.valueOf(kidId) };

            db.update(KIDS_TABLE_NAME, values, where, whereArgs);
            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Event> getEvents(String where,  String[] whereArgs, String kidsWhere, int limit) {
        List<Event> recordsList = new ArrayList<Event>();

        try {

            String selection = "e."+EVENTS_TABLE_ID_COLUMN+",e."+EVENTS_TABLE_TYPE_COLUMN+",e."+EVENTS_TABLE_KID_EXTERNALID_COLUMN+",e."+EXTERNALID_COLUMN+
                    ",e."+EVENTS_TABLE_CREATEDON_COLUMN+",e."+EVENTS_TABLE_TEACHER_EXTERNALID_COLUMN+",e."+EVENTS_TABLE_TEACHER_NAME_COLUMN+",e."+EVENTS_TABLE_IS_NEW_COLUMN;

            String selectionGroup = selection;

            selection += ",ed."+EVENT_DETAILS_TABLE_KEY_COLUMN+" as detail_key, ed."+EVENT_DETAILS_TABLE_VALUE_COLUMN+" as detail_value";
            selectionGroup += ",detail_key, detail_value";


            String sql = "";
            sql += " SELECT "+selection;
            sql += " FROM " + EVENTS_TABLE_NAME + " as e";
            sql += " JOIN " + KIDS_TABLE_NAME + " as k ON k."+EXTERNALID_COLUMN + " = e."+ EVENTS_TABLE_KID_EXTERNALID_COLUMN + " "+kidsWhere;
            sql += " JOIN " + EVENT_DETAILS_TABLE_NAME + " as ed ON ed."+EVENT_DETAILS_TABLE_EVENT_EXTERNALID_COLUMN + " = e."+EXTERNALID_COLUMN;
            sql += where;
            sql += " GROUP BY "+selectionGroup;
            sql += " ORDER BY e." + EVENTS_TABLE_CREATEDON_COLUMN + " DESC";

            //if(limit > 0)
            //    sql += " LIMIT "+limit;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();

            List<EventDetail> eventDetails = new ArrayList<EventDetail>();
            Event event = null;
            int count = 0;
            while (!cursor.isAfterLast())
            {
                int eventId = cursor.getInt(cursor.getColumnIndex(EVENTS_TABLE_ID_COLUMN));
                String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));
                String kidExternalId = cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_KID_EXTERNALID_COLUMN));
                String eventType = cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_TYPE_COLUMN));
                long eventCreatedOn = cursor.getLong(cursor.getColumnIndex(EVENTS_TABLE_CREATEDON_COLUMN));
                String teacherExternalId = cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_TEACHER_EXTERNALID_COLUMN));
                String teacherName = cursor.getString(cursor.getColumnIndex(EVENTS_TABLE_TEACHER_NAME_COLUMN));
                int isNew = cursor.getInt(cursor.getColumnIndex(EVENTS_TABLE_IS_NEW_COLUMN));

                String detailKey = cursor.getString(cursor.getColumnIndex("detail_key"));
                String detailValue = cursor.getString(cursor.getColumnIndex("detail_value"));

                if(event != null && event.getId() != eventId){
                    event.setEventDetails(eventDetails);
                    recordsList.add(event);

                    eventDetails = new ArrayList<EventDetail>();
                    event = null;
                }

                if(event == null){
                    count ++;
                    if(limit > 0 && count > limit) break;

                    event = new Event(eventId, externalId,kidExternalId,null,eventCreatedOn,eventType, teacherExternalId,teacherName, null);
                    event.setIsNew(isNew == 1);
                }
                eventDetails.add(new EventDetail(eventId,detailKey,detailValue));

                cursor.moveToNext();

            }

            if(event != null){
                event.setEventDetails(eventDetails);
                recordsList.add(event);
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;

    }

    public List<Event> readEvents(String listenerExternalId, boolean forSelectedKids, int lastEventId, int count) {
        String where = "";
        where = " WHERE e." + LISTENER_EXTERNALID_COLUMN + " = ?";

        if(forSelectedKids)
            where += " AND k." + KIDS_TABLE_IS_SELECTED_COLUMN + " = 1";

        if(lastEventId > 0)
            where += " AND e."+EVENTS_TABLE_ID_COLUMN + " < "+lastEventId;

        String kidsWhere = " AND k."+ LISTENER_EXTERNALID_COLUMN + " = ?";

        String[] whereArgs = { listenerExternalId, listenerExternalId };

        return getEvents(where, whereArgs, kidsWhere, count);
    }

    public Event getEvent(long id) {
        String where = "";
        where = " WHERE e." + EVENTS_TABLE_ID_COLUMN + " = ?";

        String[] whereArgs = { String.valueOf(id) };

        List<Event> events = getEvents(where, whereArgs, "", 0);

        if(events == null || events.size() == 0)
            return null;

        return events.get(0);
    }

    public Event getEventByExternalId(String externalId) {
        String where = "";
        where = " WHERE e." + EXTERNALID_COLUMN + " = ?";

        String[] whereArgs = { String.valueOf(externalId) };

        List<Event> events = getEvents(where, whereArgs, "", 0);

        if(events == null || events.size() == 0)
            return null;

        return events.get(0);
    }

    public long addEvent(String listenerExternalId, String childId, SyncEventResponse event) {
        long id = 0;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(EVENTS_TABLE_TYPE_COLUMN, event.action);
            values.put(EVENTS_TABLE_CREATEDON_COLUMN, event.created_at);
            values.put(EVENTS_TABLE_TEACHER_EXTERNALID_COLUMN, event.teacher.id);
            values.put(EVENTS_TABLE_TEACHER_NAME_COLUMN, event.teacher.name);
            values.put(EVENTS_TABLE_KID_EXTERNALID_COLUMN, childId);
            values.put(LISTENER_EXTERNALID_COLUMN, listenerExternalId);
            values.put(EXTERNALID_COLUMN, event.id);
            values.put(EVENTS_TABLE_IS_NEW_COLUMN, 0);

            id = db.insert(EVENTS_TABLE_NAME, null, values);

            if(!event.action.equals(ActionType.Photos)){
                for(SyncEventPropertyResponse property : event.properties)
                {
                    values.clear();

                    values.put(EVENT_DETAILS_TABLE_EVENT_EXTERNALID_COLUMN, event.id);

                    values.put(EVENT_DETAILS_TABLE_KEY_COLUMN, property.type.name);
                    values.put(EVENT_DETAILS_TABLE_VALUE_COLUMN, property.value.toString());
                    db.insert(EVENT_DETAILS_TABLE_NAME, null, values);
                }
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    public long addEventDetail(String eventExternalId, String key, String value) {
        long id = 0;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(EVENT_DETAILS_TABLE_EVENT_EXTERNALID_COLUMN, eventExternalId);
            values.put(EVENT_DETAILS_TABLE_KEY_COLUMN, key);
            values.put(EVENT_DETAILS_TABLE_VALUE_COLUMN, value);

            id = db.insert(EVENT_DETAILS_TABLE_NAME, null, values);

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return id;
    }

    public long getLastEventDate() {
        long result = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            String sql = "SELECT "+ EVENTS_TABLE_CREATEDON_COLUMN;
            sql += " FROM "+EVENTS_TABLE_NAME;
            sql += " ORDER BY "+ EVENTS_TABLE_CREATEDON_COLUMN + " DESC";
            sql += " LIMIT 1";

            Cursor cursor = db.rawQuery(sql, null);

            cursor.moveToFirst();

            while (!cursor.isAfterLast())
            {
                result = cursor.getLong(cursor.getColumnIndex(EVENTS_TABLE_CREATEDON_COLUMN));
                break;
            }

            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public long getLastActivityDate() {
        long result = 0;

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            String sql = "SELECT "+ SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN;
            sql += " FROM "+SCHEDULED_ACTIVITIES_TABLE_NAME;
            sql += " ORDER BY "+ SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN + " DESC";
            sql += " LIMIT 1";

            Cursor cursor = db.rawQuery(sql, null);

            cursor.moveToFirst();

            while (!cursor.isAfterLast())
            {
                result = cursor.getLong(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN));
                break;
            }

            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String getOrCreateSetting(String name, String value){
        String finalValue = null;

        try {

            String sql = "SELECT * FROM "+SETTINGS_TABLE_NAME+" WHERE "+SETTINGS_TABLE_NAME_COLUMN+"= '"+name+"'";

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            String oldValue = null;
            if (cursor.moveToFirst())
            {
                oldValue = cursor.getString(cursor.getColumnIndex(SETTINGS_TABLE_VALUE_COLUMN));
            }
            cursor.close();

            if(oldValue != null/* && value == null*/)
                finalValue = oldValue;
            else{
                finalValue = value;

                ContentValues values = new ContentValues();

                values.put(SETTINGS_TABLE_NAME_COLUMN, name);
                values.put(SETTINGS_TABLE_VALUE_COLUMN, value);


                //if(oldValue == null){
                db.insert(SETTINGS_TABLE_NAME, null, values);
                /*}
                else{
                    String where = SETTINGS_TABLE_NAME_COLUMN + " = ?";
                    String[] whereArgs = { name };

                    db.update(SETTINGS_TABLE_NAME, values, where, whereArgs);
                }*/
            }

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalValue;
    }
    /*
    public ArrayList<Event> getEvents(int count, int id) {
        mDatabase = mDbHelper.getWritableDatabase();
        ArrayList<Event> events = new ArrayList<Event>();

        String where = "1==1";
        if(id > 0)
            where = EVENTS_TABLE_ID_COLUMN+"="+id;

        String  query = "SELECT "+
                EVENTS_TABLE_ID_COLUMN+","+
                EVENTS_TABLE_DATE_COLUMN+","+
                EVENTS_TABLE_TITLE_COLUMN+","+
                EVENTS_TABLE_TYPE_COLUMN+
                " FROM "+EVENTS_TABLE_NAME+
                " WHERE "+where+
                " ORDER BY "+EVENTS_TABLE_DATE_COLUMN+" DESC"+
                " LIMIT "+String.valueOf(count);

        Cursor cursor = mDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int eventId = cursor.getInt(0);
            long date = cursor.getLong(1);
            String title = cursor.getString(2);
            String type = cursor.getString(3);

            events.add(new Event(eventId, date, title, type));

            cursor.moveToNext();
        }
        cursor.close();

        return events;
    }   */

    /*public Cursor getEventsCursor(int count) {
        String where = "1==1";

        String  query = "SELECT "+
                "e."+EVENTS_TABLE_ID_COLUMN+","+
                "e."+EVENTS_TABLE_FROM_COLUMN+","+
                "e."+EVENTS_TABLE_DATE_COLUMN+","+
                "e."+EVENTS_TABLE_TITLE_COLUMN+","+
                "e."+EVENTS_TABLE_TYPE_COLUMN+","+
                "n."+NOTES_TABLE_TEXT_COLUMN+
                " FROM "+EVENTS_TABLE_NAME+" as e"+
                " LEFT JOIN "+NOTES_TABLE_NAME+" as n ON e."+EVENTS_TABLE_ID_COLUMN+"=n."+NOTES_TABLE_EVENTID_COLUMN +
                " WHERE "+where+
                " ORDER BY "+EVENTS_TABLE_DATE_COLUMN+" DESC"+
                " LIMIT "+String.valueOf(count);

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(query, null);
    }
    public Note getNote(int eventId)
    {
        Note note = null;

        try{
            String  query = "SELECT "+
                    "e."+EVENTS_TABLE_ID_COLUMN+","+
                    "e."+EVENTS_TABLE_DATE_COLUMN+","+
                    "e."+EVENTS_TABLE_FROM_COLUMN+","+
                    "n."+NOTES_TABLE_TEXT_COLUMN+
                    " FROM "+EVENTS_TABLE_NAME+" as e"+
                    " JOIN "+NOTES_TABLE_NAME+" as n ON e."+EVENTS_TABLE_ID_COLUMN+"=n."+NOTES_TABLE_EVENTID_COLUMN +
                    " WHERE e."+EVENTS_TABLE_ID_COLUMN+"="+eventId;

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            if(cursor.getCount() < 1){
                cursor.close();
                return null;
            }

            cursor.moveToFirst();
            long date = cursor.getLong(1);
            String from = cursor.getString(2);
            String message = cursor.getString(3);

            note = new Note(eventId, date, from, message);

            cursor.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    return note;
    }*/

    public List<ScheduledActivity> readScheduledActivities(String listenerId, long actiId, String actiExternalId) {

        List<ScheduledActivity> recordsList = new ArrayList<ScheduledActivity>();

        String[] whereArgs = { listenerId };

        try {
            String sql = "";
            sql += " SELECT a.*";
            sql += " FROM " + SCHEDULED_ACTIVITIES_TABLE_NAME + " as a";
            sql += " WHERE a."+LISTENER_EXTERNALID_COLUMN +" = ?";

            if(actiId > 0){
                sql += " AND a."+SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN + " = '"+actiId+"'";
            }

            if(actiExternalId != null){
                sql += " AND a."+EXTERNALID_COLUMN+" = '"+actiExternalId+"'";
            }

            /*String sqlKids = "";
            sqlKids += " SELECT ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN;
            sqlKids += " FROM "+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME + " as ek";
            sqlKids += " JOIN " + KIDS_TABLE_NAME + " as k on k."+EXTERNALID_COLUMN + " = ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN;
            sqlKids += " WHERE ek."+SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " = ?";
            */

            String sqlDates = "";
            sqlDates += " SELECT *";
            sqlDates += " FROM "+SCHEDULED_ACTIVITY_DATES_TABLE_NAME;
            sqlDates += " WHERE "+SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN + " = ?";

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery(sql, whereArgs);

            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                String externalId = cursor.getString(cursor.getColumnIndex(EXTERNALID_COLUMN));
                long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN)));
                Boolean isActive = cursor.getInt(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN)) == 1;
                Integer type = cursor.getInt(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN));
                String title = cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN));
                String description = cursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN));
                long createdOn = cursor.getLong(cursor.getColumnIndex(SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN));

                ArrayList<String> kidsExternalIds = new ArrayList<String>();
                /*// kids
                String[] kidsWhereArgs = { String.valueOf(id) };
                Cursor kidsCursor = db.rawQuery(sqlKids, kidsWhereArgs);
                kidsCursor.moveToFirst();
                while (!kidsCursor.isAfterLast())
                {
                    String kidExternalId = kidsCursor.getString(kidsCursor.getColumnIndex(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN));
                    kidsExternalIds.add(kidExternalId);

                    kidsCursor.moveToNext();
                }
                kidsCursor.close();
                */
                ArrayList<ScheduledActivityDate> activityDates = new ArrayList<ScheduledActivityDate>();

                //event details
                String[] activityDatesWhereArgs = { String.valueOf(id) };
                Cursor activityDatesCursor = db.rawQuery(sqlDates, activityDatesWhereArgs);
                activityDatesCursor.moveToFirst();
                while (!activityDatesCursor.isAfterLast())
                {
                    long dateid = Long.parseLong(activityDatesCursor.getString(cursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN)));
                    long date = activityDatesCursor.getLong(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN));
                    Integer reminderType = activityDatesCursor.getInt(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN));
                    String reminderMessage = activityDatesCursor.getString(activityDatesCursor.getColumnIndex(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN));
                    String dateexternalId = cursor.getString(activityDatesCursor.getColumnIndex(EXTERNALID_COLUMN));

                    ScheduledActivityDate activityDate = new ScheduledActivityDate(dateid, dateexternalId, date, reminderType, reminderMessage);
                    activityDates.add(activityDate);
                    activityDatesCursor.moveToNext();
                }
                activityDatesCursor.close();

                ScheduledActivity activity = new ScheduledActivity(id,externalId, isActive,type,title,description,activityDates,kidsExternalIds);
                recordsList.add(activity);

                cursor.moveToNext();
            }

            cursor.close();
            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return recordsList;
    }

    public ScheduledActivity getScheduledActivity(long id) {
        List<ScheduledActivity> activities = readScheduledActivities(null, id, null);
        if(activities == null || activities.size() == 0)
            return null;

        return activities.get(0);
    }

    public long addOrUpdateScheduledActivity(ScheduledActivity sa) {
        long saId = 0;
        try{
            saId = sa.getId();
            boolean isNew = sa.getId() <= 0;
            ScheduledActivity existingActivity = null;
            if(!isNew)
                existingActivity = getScheduledActivity(saId);

            ContentValues values = new ContentValues();

            values.put(SCHEDULED_ACTIVITIES_TABLE_ISACTIVE_COLUMN, sa.getIsActive());
            values.put(SCHEDULED_ACTIVITIES_TABLE_TYPE_COLUMN, sa.getType());
            values.put(SCHEDULED_ACTIVITIES_TABLE_TITLE_COLUMN, sa.getTitle());
            values.put(SCHEDULED_ACTIVITIES_TABLE_DESCRIPTION_COLUMN, sa.getDescription());

            values.put(EXTERNALID_COLUMN, sa.getExternalId());

            SQLiteDatabase db = this.getWritableDatabase();

            if(isNew){
                values.put(SCHEDULED_ACTIVITIES_TABLE_CREATEDON_COLUMN, System.currentTimeMillis());
                saId = db.insert(SCHEDULED_ACTIVITIES_TABLE_NAME, null, values);
            }else{
                String[] whereArgs = { String.valueOf(saId) };
                String where = SCHEDULED_ACTIVITIES_TABLE_ID_COLUMN + " = ?";
                db.update(SCHEDULED_ACTIVITIES_TABLE_NAME, values, where, whereArgs);
            }

            if(sa.getDates() != null && sa.getDates().size() > 0) {

                List<Long> dateIds = new ArrayList<Long>();
                for (ScheduledActivityDate date : sa.getDates()) {
                    values.clear();

                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                    values.put(EXTERNALID_COLUMN, date.getExternalId());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_DATE_COLUMN, date.getDate());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_TYPE_COLUMN, date.getReminderType());
                    values.put(SCHEDULED_ACTIVITY_DATES_TABLE_REMINDER_MESSAGE_COLUMN, date.getReminderMessage());

                    long dateId = date.getId();
                    boolean isDateNew = dateId <= 0;

                    if (isDateNew)
                        dateId = db.insert(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, null, values);
                    else {
                        String[] whereArgs = {String.valueOf(dateId)};
                        String where = SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN + " = ?";
                        db.update(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, values, where, whereArgs);
                    }

                    dateIds.add(dateId);
                }

                if (existingActivity != null) {
                    List<Long> oldDateIds = new ArrayList<Long>();
                    List<Long> newDateIds = new ArrayList<Long>();

                    for (ScheduledActivityDate oldDate : existingActivity.getDates())
                        oldDateIds.add(oldDate.getId());
                    for (ScheduledActivityDate newDate : sa.getDates())
                        newDateIds.add(newDate.getId());


                    for (Long oldDateId : oldDateIds) {
                        if (!newDateIds.contains(oldDateId)) {
                            db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_ID_COLUMN + " = '" + oldDateId + "'", null);
                        }
                    }
                }
            }

            /*if (sa.getKidsExternalIds() != null && sa.getKidsExternalIds().size() > 0) {
                for (String newKidExternalId : sa.getKidsExternalIds()) {
                    if (existingActivity == null || !existingActivity.getKidsExternalIds().contains(newKidExternalId)) {
                        values.clear();

                        values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_SCHEDULED_ACTIVITYID_COLUMN, saId);
                        values.put(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN, newKidExternalId);
                        db.insert(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, null, values);
                    }
                }

                if (existingActivity != null) {
                    for (String oldKidExternalId : existingActivity.getKidsExternalIds()) {
                        if (!sa.getKidsExternalIds().contains(oldKidExternalId)) {
                            db.delete(SCHEDULED_ACTIVITY_TO_KIDS_TABLE_NAME, SCHEDULED_ACTIVITY_TO_KIDS_TABLE_KID_EXTERNALID_COLUMN + " = '" + oldKidExternalId + "'", null);
                        }
                    }
                }
            }*/

            db.close();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return saId;
    }

    public void deleteScheduledActivityDates(long id) {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();

            db.delete(SCHEDULED_ACTIVITY_DATES_TABLE_NAME, SCHEDULED_ACTIVITY_DATES_TABLE_SCHEDULED_ACTIVITYID_COLUMN+" = '"+id+"'", null);

            db.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}