package com.lmp.ParentMessenger.Background;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CursorAdapter;
import android.widget.TextView;
import com.lmp.ParentMessenger.R;

import java.util.Calendar;
import java.util.Date;

/*public class EventsListAdapter extends CursorAdapter
{

    // We have two list item view types

    private static final int VIEW_TYPE_GROUP_START = 0;
    private static final int VIEW_TYPE_GROUP_CONT = 1;
    private static final int VIEW_TYPE_COUNT = 2;

    public EventsListAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);

        // Get the layout inflater
        mInflater = LayoutInflater.from(context);

        // Get and cache column indices
        mColId = cursor.getColumnIndex(DatabaseHelper.EVENTS_TABLE_ID_COLUMN);
        mColFrom = cursor.getColumnIndex(DatabaseHelper.EVENTS_TABLE_FROM_COLUMN);
        mColDate = cursor.getColumnIndex(DatabaseHelper.EVENTS_TABLE_DATE_COLUMN);
        mColType = cursor.getColumnIndex(DatabaseHelper.EVENTS_TABLE_TYPE_COLUMN);

        mColMessage = cursor.getColumnIndex(DatabaseHelper.NOTES_TABLE_TEXT_COLUMN);

        Log.e("indexes",mColFrom+"/"+mColDate+"/"+mColType+"/"+mColMessage);
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        // There is always a group header for the first data item

        if (position == 0) {
            return VIEW_TYPE_GROUP_START;
        }

        // For other items, decide based on current data
        Cursor cursor = getCursor();
        cursor.moveToPosition(position);
        boolean newGroup = isNewGroup(cursor, position);

        // Check item grouping

        if (newGroup) {
            return VIEW_TYPE_GROUP_START;
        } else {
            return VIEW_TYPE_GROUP_CONT;
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        int position = cursor.getPosition();
        int nViewType;

        if (position == 0) {
            // Group header for position 0
            nViewType = VIEW_TYPE_GROUP_START;
        } else {
            // For other positions, decide based on data
            boolean newGroup = isNewGroup(cursor, position);

            if (newGroup) {
                nViewType = VIEW_TYPE_GROUP_START;
            } else {
                nViewType = VIEW_TYPE_GROUP_CONT;
            }
        }

        View v;

        String type = cursor.getString(mColType);

        int templateId = GetTemplateId(type);
        v = mInflater.inflate(templateId, parent, false);
        View vHeader = v.findViewById(R.id.event_item_header);
        vHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        if (nViewType != VIEW_TYPE_GROUP_START) {
            vHeader.setVisibility(View.GONE);
        }
        return v;
    }

    private int GetTemplateId(String type) {
        if(type == EventHelper.Note)
            return R.layout.events_list_row;

        return R.layout.events_list_row;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        //EventsViewHolder holder  =   (EventsViewHolder)view.getTag();
        //holder.
        TextView tv;

        tv = (TextView) view.findViewById(R.id.event_from);
        tv.setText(cursor.getString(mColFrom));

        tv = (TextView) view.findViewById(R.id.event_time);
        Date d = new Date(cursor.getLong(mColDate));
        tv.setText(EventHelper.DateFormaEventListItem.format(d));

        // If there is a group header, set its value to just the date
        tv = (TextView) view.findViewById(R.id.event_item_header);
        if (tv != null) {
            tv.setText(EventHelper.DateFormatIventListGroupItem.format(d));
        }

        tv = (TextView) view.findViewById(R.id.event_message);
        if(tv != null)
            tv.setText(cursor.getString(mColMessage));

    }

    private boolean isNewGroup(Cursor cursor, int position) {
        // Get date values for current and previous data items

        long nWhenThis = cursor.getLong(mColDate);

        cursor.moveToPosition(position - 1);
        long nWhenPrev = cursor.getLong(mColDate);

        // Restore cursor position

        cursor.moveToPosition(position);

        // Compare date values, ignore time values

        Calendar calThis = Calendar.getInstance();
        calThis.setTimeInMillis(nWhenThis);

        Calendar calPrev = Calendar.getInstance();
        calPrev.setTimeInMillis(nWhenPrev);

        int nDayThis = calThis.get(Calendar.DAY_OF_YEAR);
        int nDayPrev = calPrev.get(Calendar.DAY_OF_YEAR);

        if (nDayThis != nDayPrev || calThis.get(Calendar.YEAR) != calPrev.get(Calendar.YEAR)) {
            return true;
        }

        return false;
    }

    LayoutInflater mInflater;

    private int mColId;
    private int mColDate;
    private int mColFrom;
    private int mColType;

    private int mColMessage;
}

// End of MessageAdapter

/*
public class EventsListAdapter extends ArrayAdapter<Event> {

    private final Context context;
    private final int layoutResourceId;
    private final ArrayList<Event> data;
    HistoryHolder holder;

    public EventsListAdapter(Context context, int layoutResourceId, ArrayList<Event> data)
    {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        Event event = data.get(position);
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new HistoryHolder();
            holder.fromStation = (TextView)row.findViewById(R.id.historyFromStation);
            holder.toStation = (TextView)row.findViewById(R.id.historyToStation);
            holder.fromStationImage = (ImageView)row.findViewById(R.id.historyFromStationImage);
            holder.toStationImage = (ImageView)row.findViewById(R.id.historyToStationImage);

            row.setTag(holder);
        }else{
            holder = (HistoryHolder)row.getTag();
        }

        holder.fromStation.setText(bestPath.getSourceStation().getName());
        holder.toStation.setText(bestPath.getDestinationStation().getName());
        holder.fromStationImage.setImageResource(BestPathUIHelper.GetLineImageResourceId(bestPath.getSourceStation().GetConcreteLineId()));
        holder.toStationImage.setImageResource(BestPathUIHelper.GetLineImageResourceId(bestPath.getDestinationStation().GetConcreteLineId()));


        return row;
    }

    static class HistoryHolder {
        TextView fromStation;
        TextView toStation;
        ImageView fromStationImage;
        ImageView toStationImage;
    }
}
*/
