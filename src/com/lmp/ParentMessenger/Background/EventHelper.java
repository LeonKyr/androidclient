package com.lmp.ParentMessenger.Background;

import java.text.SimpleDateFormat;

public class EventHelper
{
    public static final String Note = "note";

    public static SimpleDateFormat DateFormatTodayEventListItem = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat DateFormatNotTodayEventListItem = new SimpleDateFormat("dd MMM HH:mm");

    //public static SimpleDateFormat DateFormatIventListGroupItem = new SimpleDateFormat("EEE, dd MMMM");
    public static SimpleDateFormat DateFormatIventDetails = new SimpleDateFormat("EEE, dd MMMM HH:mm");

}
