package com.lmp.ParentMessenger.Domain;

import java.util.Date;
import java.util.List;

public class Event {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private int Id;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    private String externalId;

    public String getkidExternalId() {
        return kidExternalId;
    }

    public void setkidExternalId(String kidExternalId) {
        this.kidExternalId = kidExternalId;
    }

    private String kidExternalId;

    public Media getKidImage() {
        return mImage;
    }

    public void setKidImage(Media mImage) {
        this.mImage = mImage;
    }

    private Media mImage;


    public long getCreatedOn() {
        return mCreatedOn;
    }

    public void setCreatedOn(long mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }

    private long mCreatedOn;

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    private String mType;

    public String getTeacherId() {
        return mTeacherId;
    }

    public void setTeacherId(String mTeacherId) {
        this.mTeacherId = mTeacherId;
    }

    private String mTeacherId;

    public String getTeacherName() {
        return mTeacherName;
    }

    public void setTeacherName(String mTeacherName) {
        this.mTeacherName = mTeacherName;
    }

    private String mTeacherName;

    public List<EventDetail> getEventDetails() {
        return mEventDetails;
    }

    public void setEventDetails(List<EventDetail> eventDetails) {
        this.mEventDetails = eventDetails;
    }

    private List<EventDetail> mEventDetails;


    public String GetDetailValue(String key){
        for(EventDetail detail:mEventDetails){
            if(detail.getKey().equals(key))
                return detail.getValue();
        }

        return null;
    }

    public Boolean getIsNew() {
        return IsNew;
    }

    public void setIsNew(Boolean isNew) {
        IsNew = isNew;
    }

    private Boolean IsNew;

    public Event(int id, String externalId, String kidExternalId, Media kidImage, long createdOn, String type, String teacherExternalId, String teacherName, List<EventDetail> eventDetails)
    {
        setId(id);
        setExternalId(externalId);
        setkidExternalId(kidExternalId);
        setKidImage(kidImage);
        setCreatedOn(createdOn);
        setType(type);
        setTeacherId(teacherExternalId);
        setTeacherName(teacherName);
        setEventDetails(eventDetails);
    }
}
