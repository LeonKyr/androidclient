package com.lmp.ParentMessenger.Domain;

public class ScheduledActivityDate implements Comparable {
    private long id;
    private long date;
    private int reminderType;
    private String reminderMessage;
    private String mExternalId;

    public ScheduledActivityDate(long id, String externalId, long date, int reminderType, String reminderMessage) {
        this.id = id;
        this.date = date;
        this.reminderType = reminderType;
        this.reminderMessage = reminderMessage;
        this.mExternalId = externalId;
    }

    public long getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public int getReminderType() {
        return reminderType;
    }

    public String getReminderMessage() {
        return reminderMessage;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setReminderType(int reminderType) {
        this.reminderType = reminderType;
    }

    public void setReminderMessage(String reminderMessage) {
        this.reminderMessage = reminderMessage;
    }

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    @Override
    public int compareTo(Object date) {
        if(this.date < ((ScheduledActivityDate)date).getDate())
            return -1;

        if(this.date == ((ScheduledActivityDate)date).getDate())
            return 0;

        return 1;
    }
}
