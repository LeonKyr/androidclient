package com.lmp.ParentMessenger.Domain;

public class EventDetail {
    public int getEventId() {
        return mEventId;
    }

    public void setEventId(int mId) {
        this.mEventId = mId;
    }

    private int mEventId;

    public String getKey() {
        return mKey;
    }

    public void setKey(String mKey) {
        this.mKey = mKey;
    }

    private String mKey;

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    private String mValue;

    public EventDetail(int eventId, String key, String value){
        setEventId(eventId);
        setKey(key);
        setValue(value);
    }

    public EventDetail(String key, String value){
        this(0, key,value);
    }
}
