package com.lmp.ParentMessenger.Domain;

public class Listener extends SynchronizableBase {

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    private int Id;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    private String mEmail;

    public boolean getIsAutoLogin() {
        return mIsAutoLogin;
    }

    public void setIsAutoLogin(boolean mIsAutoLogin) {
        this.mIsAutoLogin = mIsAutoLogin;
    }

    private boolean mIsAutoLogin;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    private String mPassword;

    public Media getImage() {
        return mImage;
    }

    public void setImage(Media mImage) {
        this.mImage = mImage;
    }

    private Media mImage;

    public Listener(String externalId, int id, String name, String email, String hashPassword, boolean isAutoLogin, Media image)
    {
        super(externalId);

        setId(id);
        setName(name);
        setEmail(email);
        setIsAutoLogin(isAutoLogin);
        setPassword(hashPassword);
        setImage(image);
    }
}