package com.lmp.ParentMessenger.Domain;

public class Kid extends SynchronizableBase {
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    private int mId;

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    private String mName;

    public long getBirthday() {
        return mBirthday;
    }

    public void setBirthday(long mBirthday) {
        this.mBirthday = mBirthday;
    }

    private long mBirthday;

    public Media getImage() {
        return mImage;
    }

    public void setImage(Media mImage) {
        this.mImage = mImage;
    }

    private Media mImage;

    public String getExternalId() {
        return mExternalId;
    }

    public void setExternalId(String mExternalId) {
        this.mExternalId = mExternalId;
    }

    private String mExternalId;

    public String getUniqueId() {
        return mUniqueId;
    }

    public void setUniqueId(String mUniqueId) {
        this.mUniqueId = mUniqueId;
    }

    private String mUniqueId;

    public boolean getIsSelected() {
        return mIsSelected;
    }

    public void setIsSelected(boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }

    private boolean mIsSelected;

    public Kid(String listenerExternalId, int id, String externalId, String name, Media image, long birthday, String uniqueId, boolean isSelected)
    {
        super(listenerExternalId);
        setId(id);
        setExternalId(externalId);
        setName(name);
        setBirthday(birthday);
        setImage(image);
        setUniqueId(uniqueId);
        setIsSelected(isSelected);
    }

    public Kid(String listenerExternalId){
        super(listenerExternalId);
    }
}