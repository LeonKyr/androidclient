package com.lmp.ParentMessenger.Domain;

public abstract class SynchronizableBase
{
    public String getExternalListenerId() {
        return mListenerId;
    }

    public void setExternalListenerId(String mListenerId) {
        this.mListenerId = mListenerId;
    }

    private String mListenerId;


    public SynchronizableBase(String listenerId){
        setExternalListenerId(listenerId);
    }
}